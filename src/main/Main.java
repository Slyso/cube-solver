package main;

import lejos.hardware.Button;
import movement.ExecuteSolution;
import movement.MoveArm;
import movement.MoveColorSensor;
import scan.Scan;
import sensor.ColorSensor;
import sensor.IrSensor;
import solve.Solve;
import util.Cube;
import util.EditCube;
import util.PrintString;
import util.Timer;

public class Main
{
	private static long scanTime, calculationTime, executionTime;

	public static void main(String[] args)
	{
		resetMotorPositions();
		IrSensor.waitForCube();

		Cube cube = scanAndEditCube();
		String solution;

		while (true)
		{
			Timer.start();
			solution = Solve.solve(cube);
			calculationTime = Timer.getPassedTime();

			if (solution == null) cube = EditCube.editCube(cube);
			else break;
		}

		Timer.start();
		ExecuteSolution.execute(solution);
		executionTime = Timer.getPassedTime();

		displayPassedTimes();
	}

	private static Cube scanAndEditCube()
	{
		ColorSensor.start();
		ColorSensor.initColorValues();
		Timer.start();

		Cube cube = new Cube(Scan.scanCube());
		ColorSensor.close();

		scanTime = Timer.getPassedTime();

		cube = EditCube.editCube(cube);
		return cube;
	}

	private static void displayPassedTimes()
	{
		long totalTime = scanTime + calculationTime + executionTime;

		PrintString.print("Total time: " + totalTime, true);
		PrintString.print("Scan time: " + scanTime, true);
		PrintString.print("Calculation time: " + calculationTime, true);
		PrintString.print("Execution time: " + executionTime, true);
	}

	private static void resetMotorPositions()
	{
		Button.LEDPattern(2); //Red static
		MoveColorSensor.reset();
		MoveArm.reset();
	}
}
