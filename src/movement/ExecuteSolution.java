package movement;

import java.util.ArrayList;

import lejos.hardware.Button;
import util.PrintString;
import util.SplitSolution;

public class ExecuteSolution
{
	//Orientation after the scan
	private static int upperFace = 5;
	private static int frontFace = 0;

	public static void execute(String solution)
	{
		ArrayList moves = SplitSolution.splitSolution(solution);
		Button.LEDPattern(7); //Green flashing

		for (int i = 0; i < moves.size(); i++)
		{
			if (Button.ENTER.isDown()) RotateCube.floatMode();
			PrintString.print((String) moves.get(i), false);
			applyMove((String) moves.get(i));
		}

		Button.LEDPattern(1); //Green static
	}

	private static void applyMove(String turn)
	{
		orientateCube(turn);

		if (turn.contains("2")) TurnLayer.full();
		else if (turn.contains("'")) TurnLayer.counterclockwise();
		else TurnLayer.clockwise();
	}

	private static void orientateCube(String turn)
	{
		if (frontFace == 5)
		{
			if (turn.contains("D"))
			{
				if (!turnCube(0 - upperFace, "D")) return;
				upperFace = 0;
			}
			else if (turn.contains("L"))
			{
				if (!turnCube(1 - upperFace, "L")) return;
				upperFace = 1;
			}
			else if (turn.contains("U"))
			{
				if (!turnCube(2 - upperFace, "U")) return;
				upperFace = 2;
			}
			else if (turn.contains("R"))
			{
				if (!turnCube(3 - upperFace, "R")) return;
				upperFace = 3;
			}
			else if (turn.contains("F"))
			{
				RotateCube.clockwise();
				turnCube(1, null);
				frontFace = upperFace + 1;
				if (frontFace > 3) frontFace = 0;
				upperFace = 4;
			}
			else if (turn.contains("B"))
			{
				RotateCube.counterclockwise();
				turnCube(1, null);
				frontFace = upperFace - 1;
				if (frontFace < 0) frontFace = 3;
				upperFace = 5;
			}
		}
		else if (frontFace == 4)
		{
			if (turn.contains("D"))
			{
				if (!turnCube(upperFace, "D")) return;
				upperFace = 0;
			}
			else if (turn.contains("R"))
			{
				if (!turnCube(upperFace + 1, "R")) return;
				upperFace = 3;
			}
			else if (turn.contains("U"))
			{
				if (!turnCube(upperFace + 2, "U")) return;
				upperFace = 2;
			}
			else if (turn.contains("L"))
			{
				if (!turnCube(upperFace + 3, "L")) return;
				upperFace = 1;
			}
			else if (turn.contains("F"))
			{
				RotateCube.counterclockwise();
				turnCube(1, null);
				frontFace = upperFace + 1;
				if (frontFace > 3) frontFace = 0;
				upperFace = 4;
			}
			else if (turn.contains("B"))
			{
				RotateCube.clockwise();
				turnCube(1, null);
				frontFace = upperFace - 1;
				if (frontFace < 0) frontFace = 3;
				upperFace = 5;
			}
		}
		else if (frontFace == 3)
		{
			int[] faceOrder = { 0, 4, 2, 5 };
			int amount = 0;
			for (int i = 0; i < faceOrder.length; i++)
			{
				if (upperFace == faceOrder[i])
				{
					amount = i;
					break;
				}
			}

			if (turn.contains("D"))
			{
				if (!turnCube(amount, "D")) return;
				upperFace = 0;
			}
			else if (turn.contains("B"))
			{
				if (!turnCube(amount + 1, "B")) return;
				upperFace = 5;
			}
			else if (turn.contains("U"))
			{
				if (!turnCube(amount + 2, "U")) return;
				upperFace = 2;
			}
			else if (turn.contains("F"))
			{
				if (!turnCube(amount + 3, "F")) return;
				upperFace = 4;
			}
			else if (turn.contains("R"))
			{
				RotateCube.counterclockwise();
				turnCube(1, null);

				int frontFaceIndex = amount + 1;
				if (frontFaceIndex > faceOrder.length - 1) frontFaceIndex = 0;
				frontFace = faceOrder[frontFaceIndex];
				upperFace = 3;
			}
			else if (turn.contains("L"))
			{
				RotateCube.clockwise();
				turnCube(1, null);

				int frontFaceIndex = amount - 1;
				if (frontFaceIndex < 0) frontFaceIndex = faceOrder.length - 1;
				frontFace = faceOrder[frontFaceIndex];
				upperFace = 1;
			}
		}
		else if (frontFace == 1)
		{
			int[] faceOrder = { 0, 5, 2, 4 };
			int amount = 0;
			for (int i = 0; i < faceOrder.length; i++)
			{
				if (upperFace == faceOrder[i])
				{
					amount = i;
					break;
				}
			}

			if (turn.contains("D"))
			{
				if (!turnCube(amount, "D")) return;
				upperFace = 0;
			}
			else if (turn.contains("F"))
			{
				if (!turnCube(amount + 1, "F")) return;
				upperFace = 4;
			}
			else if (turn.contains("U"))
			{
				if (!turnCube(amount + 2, "U")) return;
				upperFace = 2;
			}
			else if (turn.contains("B"))
			{
				if (!turnCube(amount + 3, "B")) return;
				upperFace = 5;
			}
			else if (turn.contains("L"))
			{
				RotateCube.counterclockwise();
				turnCube(1, null);

				int frontFaceIndex = amount + 1;
				if (frontFaceIndex > faceOrder.length - 1) frontFaceIndex = 0;
				frontFace = faceOrder[frontFaceIndex];
				upperFace = 1;
			}
			else if (turn.contains("R"))
			{
				RotateCube.clockwise();
				turnCube(1, null);

				int frontFaceIndex = amount - 1;
				if (frontFaceIndex < 0) frontFaceIndex = faceOrder.length - 1;
				frontFace = faceOrder[frontFaceIndex];
				upperFace = 3;
			}
		}
		else if (frontFace == 2)
		{
			int[] faceOrder = { 5, 3, 4, 1 };
			int amount = 0;
			for (int i = 0; i < faceOrder.length; i++)
			{
				if (upperFace == faceOrder[i])
				{
					amount = i;
					break;
				}
			}

			if (turn.contains("B"))
			{
				if (!turnCube(amount, "B")) return;
				upperFace = 5;
			}
			else if (turn.contains("L"))
			{
				if (!turnCube(amount + 1, "L")) return;
				upperFace = 1;
			}
			else if (turn.contains("F"))
			{
				if (!turnCube(amount + 2, "F")) return;
				upperFace = 4;
			}
			else if (turn.contains("R"))
			{
				if (!turnCube(amount + 3, "R")) return;
				upperFace = 3;
			}
			else if (turn.contains("U"))
			{
				RotateCube.counterclockwise();
				turnCube(1, null);

				int frontFaceIndex = amount + 1;
				if (frontFaceIndex > faceOrder.length - 1) frontFaceIndex = 0;
				frontFace = faceOrder[frontFaceIndex];
				upperFace = 2;
			}
			else if (turn.contains("D"))
			{
				RotateCube.clockwise();
				turnCube(1, null);

				int frontFaceIndex = amount - 1;
				if (frontFaceIndex < 0) frontFaceIndex = faceOrder.length - 1;
				frontFace = faceOrder[frontFaceIndex];
				upperFace = 0;
			}
		}
		else if (frontFace == 0)
		{
			int[] faceOrder = { 5, 1, 4, 3 };
			int amount = 0;
			for (int i = 0; i < faceOrder.length; i++)
			{
				if (upperFace == faceOrder[i])
				{
					amount = i;
					break;
				}
			}

			if (turn.contains("B"))
			{
				if (!turnCube(amount, "B")) return;
				upperFace = 5;
			}
			else if (turn.contains("L"))
			{
				if (!turnCube(amount + 3, "L")) return;
				upperFace = 1;
			}
			else if (turn.contains("F"))
			{
				if (!turnCube(amount + 2, "F")) return;
				upperFace = 4;
			}
			else if (turn.contains("R"))
			{
				if (!turnCube(amount + 1, "R")) return;
				upperFace = 3;
			}
			else if (turn.contains("D"))
			{
				RotateCube.counterclockwise();
				turnCube(1, null);

				int frontFaceIndex = amount + 1;
				if (frontFaceIndex > faceOrder.length - 1) frontFaceIndex = 0;
				frontFace = faceOrder[frontFaceIndex];
				upperFace = 0;
			}
			else if (turn.contains("U"))
			{
				RotateCube.clockwise();
				turnCube(1, null);

				int frontFaceIndex = amount - 1;
				if (frontFaceIndex < 0) frontFaceIndex = faceOrder.length - 1;
				frontFace = faceOrder[frontFaceIndex];
				upperFace = 2;
			}
		}

	}

	/**
	 * Turn the cube.
	 * @param amount How many times to turn the cube
	 * @param turn What turn is getting applied to the cube
	 * @return False if the turn wasn't applied to the cube yet and the method orientateCube gets executed once more
	 */
	private static boolean turnCube(int amount, String turn)
	{
		if (amount < 0) amount += 4;
		if (amount > 3) amount -= 4;

		if (amount == 3)
		{
			rotateCubeFull();
			orientateCube(turn);
			return false;
		}

		while (amount > 0)
		{
			MoveArm.tiltCube();
			amount--;
		}
		return true;
	}

	private static void rotateCubeFull()
	{
		RotateCube.full();
		if (frontFace == 5) frontFace = 4;
		else if (frontFace == 4) frontFace = 5;
		else if (frontFace == 3) frontFace = 1;
		else if (frontFace == 2) frontFace = 0;
		else if (frontFace == 1) frontFace = 3;
		else if (frontFace == 0) frontFace = 2;
	}
}
