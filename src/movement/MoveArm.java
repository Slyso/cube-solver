package movement;

import lejos.hardware.motor.Motor;
import lejos.utility.Delay;

public class MoveArm
{
	/**
	 * Move the arm into the starting position.
	 */
	public static void reset()
	{
		Motor.A.setSpeed(100);

		Motor.A.backward();
		while (!Motor.A.isStalled())
		{
		}
		Motor.A.stop();

		Motor.A.rotate(15, false);
	}

	/**
	 * Tilt the Rubic's Cube.
	 */
	public static void tiltCube()
	{
		Motor.A.setSpeed(500);
		Motor.A.rotate(170);
		Delay.msDelay(250);
		Motor.A.setSpeed(350);
		Motor.A.rotate(-170);
	}

	/**
	 * Move the arm onto the cube. Use to turn a surface of the cube.
	 */
	public static void moveOnCube()
	{
		Motor.A.setSpeed(500);
		Motor.A.rotate(95);
	}

	/**
	 * Use to move the arm back into the starting position if the arm is on the cube.
	 */
	public static void moveBack()
	{
		Motor.A.setSpeed(350);
		Motor.A.rotate(-95);
	}
}
