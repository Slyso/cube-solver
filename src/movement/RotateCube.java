package movement;

import lejos.hardware.Button;
import lejos.hardware.motor.Motor;
import lejos.utility.Delay;

public class RotateCube
{
	/**
	 * Rotate the Rubic's Cube 90� counterclockwise
	 */
	public static void counterclockwise()
	{
		//Rotating the motor by 3 * 360� will rotate the cube by 360�
		Motor.B.setSpeed(500);// 1 RPM
		Motor.B.rotate(-3 * 360 / 4);
	}

	/**
	 * Rotate the Rubic's Cube 90� clockwise
	 */
	public static void clockwise()
	{
		//Rotating the motor by 3 * 360� will rotate the cube by 360�
		Motor.B.setSpeed(500);// 1 RPM
		Motor.B.rotate(3 * 360 / 4);
	}

	/**
	 * Rotate the Rubic's Cube by 180�
	 */
	public static void full()
	{
		//Rotating the motor by 3 * 360� will rotate the cube by 360�
		Motor.B.setSpeed(500);// 1 RPM
		Motor.B.rotate(3 * 360 / 2);
	}

	/**
	 * Rotate the Rubic's Cube about 45� clockwise.
	 * 
	 * @param numberOfRotations
	 *            How many times the cube has been rotated on that surface before.
	 */
	public static void scanning(int numberOfRotations)
	{
		//Move the first sticker into place
		if (numberOfRotations == 0)
		{
			Motor.B.setSpeed(100);
			Motor.B.rotate(11 * -3, true);
			return;
		}

		Motor.B.setSpeed(350);

		if (numberOfRotations == 1) Motor.B.rotate(41 * -3, true);
		else if (numberOfRotations == 2 || numberOfRotations == 3) Motor.B.rotate(44 * -3, true);
		else if (numberOfRotations == 4 || numberOfRotations == 6 || numberOfRotations == 7)
			Motor.B.rotate(45 * -3, true);
		else if (numberOfRotations == 5) Motor.B.rotate(50 * -3, true);
		else if (numberOfRotations == 8) Motor.B.rotate(35 * -3, true);
	}

	public static boolean motorIsMoving()
	{
		return Motor.B.isMoving();
	}

	public static void floatMode()
	{
		Motor.B.stop();
		Delay.msDelay(300);
		Motor.B.flt();
		while (Button.ENTER.isUp())
		{
			Delay.msDelay(50);
		}
	}
}
