package movement;

public class TurnLayer
{
	/**
	 * Turn the bottom layer of the Rubic's Cube 90� clockwise
	 */
	public static void clockwise()
	{
		MoveArm.moveOnCube();
		RotateCube.counterclockwise();
		MoveArm.moveBack();
	}

	/**
	 * Turn the bottom layer of the Rubic's Cube 90� counterclockwise
	 */
	public static void counterclockwise()
	{
		MoveArm.moveOnCube();
		RotateCube.clockwise();
		MoveArm.moveBack();
	}

	/**
	 * Turn the bottom layer of the Rubic's Cube by 180�
	 */
	public static void full()
	{
		MoveArm.moveOnCube();
		RotateCube.full();
		MoveArm.moveBack();
	}
}
