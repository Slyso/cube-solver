package scan;

import lejos.hardware.Button;
import movement.MoveArm;
import movement.MoveColorSensor;
import movement.RotateCube;
import sensor.ColorSensor;

public class Scan
{
	private static String[][] cube = { { "", "", "", "", "", "", "", "", "" }, { "", "", "", "", "", "", "", "", "" },
			{ "", "", "", "", "", "", "", "", "" }, { "", "", "", "", "", "", "", "", "" },
			{ "", "", "", "", "", "", "", "", "" }, { "", "", "", "", "", "", "", "", "" } };

	/**
	 * Scan the whole cube.
	 * @return String[][] cube
	 */
	public static String[][] scanCube()
	{
		Button.LEDPattern(5); //Red flashing

		cube[0] = scanSurface("w", 0);
		MoveArm.tiltCube();
		cube[1] = scanSurface("r", 1);
		MoveArm.tiltCube();
		cube[2] = scanSurface("y", 2);
		MoveArm.tiltCube();
		cube[3] = scanSurface("o", 3);
		RotateCube.clockwise();
		MoveArm.tiltCube();
		cube[4] = scanSurface("b", 4);
		MoveArm.tiltCube();
		MoveArm.tiltCube();
		cube[5] = scanSurface("g", 5);

		Button.LEDPattern(0); //Turn LED off

		return cube;
	}

	/**
	 * Scan the surface of the cube with the defined center.
	 * @param center Color of the center
	 * @param surfaceNumber 0 - 5
	 * @return
	 */
	private static String[] scanSurface(String center, int surfaceNumber)
	{
		String[] colors = { "", "", "", "", "", "", "", "" };

		MoveColorSensor.move();
		RotateCube.scanning(0);

		for (int i = 0; i < 8; i++)
		{
			if (i % 2 == 0) MoveColorSensor.moveOverEdge(i == 4);
			else MoveColorSensor.moveOverCorner();

			while (RotateCube.motorIsMoving())
			{
			}

			colors[i] = ColorSensor.getColor();
			RotateCube.scanning(i + 1);
		}

		MoveColorSensor.move();
		return sortColors(colors, center, surfaceNumber);
	}

	/**
	 * Before:	<br>
	 * 7 0 1	<br>
	 * 6 c 2	<br>
	 * 5 4 3	<br>
	 * 
	 * After:	<br>
	 * 0 1 2	<br>
	 * 3 4 5	<br>
	 * 6 7 8	<br>
	 * 
	 * @param colors Unsorted string of colors
	 * @param center Color of the center
	 * @return sorted string
	 */
	private static String[] sortColors(String[] colors, String center, int surfaceNumber)
	{
		String[] surface = { colors[5], colors[6], colors[7], colors[4], center, colors[0], colors[3], colors[2],
				colors[1] };

		if (surfaceNumber < 4) return surface;

		String[] surface2 = { colors[7], colors[0], colors[1], colors[6], center, colors[2], colors[5], colors[4],
				colors[3] };

		return surface2;
	}
}
