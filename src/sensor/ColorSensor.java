package sensor;

import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.SampleProvider;
import util.ReadColorValues;

public class ColorSensor
{
	private static EV3ColorSensor colorSensor;
	private static SampleProvider colorProvider;
	private static Port port;
	private static float[] colorSample;

	// Order as in colorID, used for getColor(), the initialized values are the default values
	//Got these values by using the colorSensorUtil.getRGBRange() method
	private static float[][] colors = { { 0.039F, 0.120F, 0.100F }, { 0.046F, 0.197F, 0.133F },
			{ 0.260F, 0.315F, 0.243F }, { 0.222F, 0.048F, 0.018F }, { 0.287F, 0.328F, 0.185F },
			{ 0.270F, 0.304F, 0.246F } };
	private static String[] colorID = { "b", "g", "o", "r", "w", "y" };

	/**
	 * Initialize the sensor. Must be done before using the sensor.
	 */
	public static void start()
	{
		port = LocalEV3.get().getPort("S2");
		colorSensor = new EV3ColorSensor(port);
		colorProvider = colorSensor.getRGBMode();
		colorSample = new float[3];
	}

	/**
	 * Try to read the RGB values from the file created by WriteColorValues
	 */
	public static void initColorValues()
	{
		//If WriteColorValues wasn't run yet it will tell the user and use the default values
		float[][] values = ReadColorValues.readFile();
		if (values == null) return;
		colors = ReadColorValues.readFile();
	}

	/**
	 * Measure the intensity of Red, Green and Blue.
	 * 
	 * @return The color which is the closest to the reference color as a string
	 *         with the beginning letter ("b", "g", "o", "r", "w", "y")
	 */
	public static String getColor()
	{
		float[] distance = new float[6];

		for (int i = 0; i < 3; i++)
		{
			colorProvider.fetchSample(colorSample, 0);

			for (int colorId = 0; colorId < 6; colorId++)
			{
				for (int rgb = 0; rgb < 3; rgb++)
				{
					distance[colorId] += (float) Math.pow(colorSample[rgb] - colors[colorId][rgb], 2);
				}
			}
		}

		float smallestDistance = Float.MAX_VALUE;
		int smallestDistanceIndex = 0;

		for (int i = 0; i < distance.length; i++)
		{
			if (distance[i] < smallestDistance)
			{
				smallestDistance = distance[i];
				smallestDistanceIndex = i;
			}
		}

		return colorID[smallestDistanceIndex];
	}

	/**
	 * Close the sensor.
	 */
	public static void close()
	{
		colorSensor.close();
	}

	public static String[] getcolorID()
	{
		return colorID;
	}

	public static EV3ColorSensor getColorSensor()
	{
		return colorSensor;
	}
}
