package sensor;

import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import util.PrintString;

public class IrSensor
{
	private static EV3IRSensor irSensor;
	private static SampleProvider irProvider;
	private static Port port;
	private static float[] irSample;

	/**
	 * This method runs until the cube is in position.
	 */
	public static void waitForCube()
	{
		start();
		Button.LEDPattern(1); //Green static

		float distance = getDistance();

		while (distance > 10 || distance < 7)
		{
			PrintString.print("Please insert the cube with white on top and green in front.", false);
			Delay.msDelay(1000);
			distance = getDistance();
		}

		close();
	}

	private static void start()
	{
		port = LocalEV3.get().getPort("S1");
		irSensor = new EV3IRSensor(port);
		irProvider = irSensor.getDistanceMode();
		irSample = new float[irProvider.sampleSize()];
	}

	private static float getDistance()
	{
		irSensor.fetchSample(irSample, 0);
		return irSample[0];
	}

	private static void close()
	{
		irSensor.close();
	}
}
