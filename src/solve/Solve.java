package solve;

import java.util.ArrayList;

import util.Cube;
import util.SplitSolution;
import util.Sticker;
import util.Timeout;

public class Solve
{
	private static String solution = "";
	private static boolean addMovesToSolution = true;

	/**
	 * Solve the specified cube.
	 * @param cube
	 * @return The moves used to solve the cube. Null if no solution got found.
	 */
	public static String solve(Cube cube)
	{
		solution = "";

		solveCross(cube);
		solveF2L(cube);
		if (!solveOLL(cube))
		{
			undoMultipleTurns(solution, cube, null);
			return null;
		}
		if (!solvePLL(cube))
		{
			undoMultipleTurns(solution, cube, null);
			return null;
		}
		rotateLastLayer(cube);

		if (!cube.isSolved())
		{
			undoMultipleTurns(solution, cube, null);
			return null;
		}
		return shortenSolution();
	}

	/**
	 * Try to solve the cross. Times out if it takes to long, because it's an invalid scramble.
	 * @param cube
	 */
	private static void solveCross(Cube cube)
	{
		Timeout.timedOutIn(1000);

		Sticker cs; //Current sticker

		while (cube.getUnsolvedCrossSticker() != null)
		{
			if (Timeout.timedOut()) return;

			cs = cube.getUnsolvedCrossSticker();

			//Down
			if (cs.getSurfaceNumber() == 2)
			{
				if (cube.crossStickerMatchesSurface(cs)) continue; //This edge piece is already in the right position

				if (cs.getStickerNumber() == 1) makeTurn("B2", cube, cs);
				else if (cs.getStickerNumber() == 3) makeTurn("R2", cube, cs);
				else if (cs.getStickerNumber() == 5) makeTurn("L2", cube, cs);
				else if (cs.getStickerNumber() == 7) makeTurn("F2", cube, cs);
			}
			//Up
			if (cs.getSurfaceNumber() == 0)
			{
				for (int j = 0; j < 4; j++)
				{
					if (cube.crossStickerMatchesSurface(cs))
					{
						if (cs.getStickerNumber() == 1) makeTurn("B2", cube, cs);
						else if (cs.getStickerNumber() == 3) makeTurn("L2", cube, cs);
						else if (cs.getStickerNumber() == 5) makeTurn("R2", cube, cs);
						else if (cs.getStickerNumber() == 7) makeTurn("F2", cube, cs);
						break;
					}
					makeTurn("U", cube, cs);
				}
			}
			//Front
			if (cs.getSurfaceNumber() == 5)
			{
				if (cs.getStickerNumber() == 1)
				{
					makeTurn("L'", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("L", cube, cs);
				}
				else if (cs.getStickerNumber() == 7)
				{
					makeTurn("R", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("R'", cube, cs);
				}
				else if (cs.getStickerNumber() == 3)
				{
					makeTurn("F'", cube, cs);
					makeTurn("R", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("R'", cube, cs);
				}
				else if (cs.getStickerNumber() == 5)
				{
					makeTurn("F", cube, cs);
					makeTurn("R", cube, cs);
					makeTurn("U'", cube, cs);
					makeTurn("R'", cube, cs);
					makeTurn("F'", cube, cs);
				}
			}
			//Back
			if (cs.getSurfaceNumber() == 4)
			{
				if (cs.getStickerNumber() == 1)
				{
					makeTurn("R'", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("R", cube, cs);
				}
				else if (cs.getStickerNumber() == 7)
				{
					makeTurn("L", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("L'", cube, cs);
				}
				else if (cs.getStickerNumber() == 3)
				{
					makeTurn("B", cube, cs);
					makeTurn("R'", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("R", cube, cs);
				}
				else if (cs.getStickerNumber() == 5)
				{
					makeTurn("B'", cube, cs);
					makeTurn("R'", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("R", cube, cs);
					makeTurn("B", cube, cs);
				}
			}
			//Left
			if (cs.getSurfaceNumber() == 3)
			{
				if (cs.getStickerNumber() == 1)
				{
					makeTurn("B'", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("B", cube, cs);
				}
				else if (cs.getStickerNumber() == 7)
				{
					makeTurn("F", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("F'", cube, cs);
				}
				else if (cs.getStickerNumber() == 3)
				{
					makeTurn("L'", cube, cs);
					makeTurn("F", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("F'", cube, cs);
				}
				else if (cs.getStickerNumber() == 5)
				{
					makeTurn("L", cube, cs);
					makeTurn("F", cube, cs);
					makeTurn("U'", cube, cs);
					makeTurn("F'", cube, cs);
					makeTurn("L'", cube, cs);
				}
			}
			//Right
			if (cs.getSurfaceNumber() == 1)
			{
				if (cs.getStickerNumber() == 1)
				{
					makeTurn("B", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("B'", cube, cs);
				}
				else if (cs.getStickerNumber() == 7)
				{
					makeTurn("F'", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("F", cube, cs);
				}
				else if (cs.getStickerNumber() == 3)
				{
					makeTurn("R'", cube, cs);
					makeTurn("F'", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("F", cube, cs);
					makeTurn("R", cube, cs);
				}
				else if (cs.getStickerNumber() == 5)
				{
					makeTurn("R'", cube, cs);
					makeTurn("F'", cube, cs);
					makeTurn("U", cube, cs);
					makeTurn("F", cube, cs);
				}
			}
		}
	}

	/**
	 * Try to solve F2L. Times out if it takes to long, because it's an invalid scramble.
	 * @param cube
	 */
	private static void solveF2L(Cube cube)
	{
		Timeout.timedOutIn(1000);

		Sticker cs; //Current sticker

		//Corners
		while (cube.getUnsolvedF2LCornerSticker() != null)
		{
			if (Timeout.timedOut()) return;

			cs = cube.getUnsolvedF2LCornerSticker();

			if (cs.isOnUpperLayer())
			{
				//Bring piece in position
				for (int j = 0; j < 4; j++)
				{
					if (cube.cornerStickerMatchesSurface(cs))
					{
						break;
					}
					makeTurn("U", cube, cs);
				}

				if (cs.getSurfaceNumber() == 1)
				{
					if (cs.getStickerNumber() == 0)
					{
						makeTurn("B'", cube, cs);
						makeTurn("R", cube, cs);
						makeTurn("B", cube, cs);
						makeTurn("R'", cube, cs);
					}
					else if (cs.getStickerNumber() == 6)
					{
						makeTurn("F", cube, cs);
						makeTurn("R'", cube, cs);
						makeTurn("F'", cube, cs);
						makeTurn("R", cube, cs);
					}
				}
				if (cs.getSurfaceNumber() == 3)
				{
					if (cs.getStickerNumber() == 2)
					{
						makeTurn("B", cube, cs);
						makeTurn("L'", cube, cs);
						makeTurn("B'", cube, cs);
						makeTurn("L", cube, cs);
					}
					else if (cs.getStickerNumber() == 8)
					{
						makeTurn("F'", cube, cs);
						makeTurn("L", cube, cs);
						makeTurn("F", cube, cs);
						makeTurn("L'", cube, cs);
					}
				}
				if (cs.getSurfaceNumber() == 4)
				{
					if (cs.getStickerNumber() == 2)
					{
						makeTurn("R", cube, cs);
						makeTurn("B'", cube, cs);
						makeTurn("R'", cube, cs);
						makeTurn("B", cube, cs);
					}
					else if (cs.getStickerNumber() == 8)
					{
						makeTurn("L'", cube, cs);
						makeTurn("B", cube, cs);
						makeTurn("L", cube, cs);
						makeTurn("B'", cube, cs);
					}
				}
				if (cs.getSurfaceNumber() == 5)
				{
					if (cs.getStickerNumber() == 2)
					{
						makeTurn("L", cube, cs);
						makeTurn("F'", cube, cs);
						makeTurn("L'", cube, cs);
						makeTurn("F", cube, cs);
					}
					else if (cs.getStickerNumber() == 8)
					{
						makeTurn("R'", cube, cs);
						makeTurn("F", cube, cs);
						makeTurn("R", cube, cs);
						makeTurn("F'", cube, cs);
					}
				}
				if (cs.getSurfaceNumber() == 0)
				{
					if (cs.getStickerNumber() == 0)
					{
						makeTurn("L", cube, cs);
						makeTurn("U'", cube, cs);
						makeTurn("L'", cube, cs);
					}
					else if (cs.getStickerNumber() == 2)
					{
						makeTurn("B", cube, cs);
						makeTurn("U'", cube, cs);
						makeTurn("B'", cube, cs);
					}
					else if (cs.getStickerNumber() == 6)
					{
						makeTurn("L'", cube, cs);
						makeTurn("U", cube, cs);
						makeTurn("L", cube, cs);
					}
					else if (cs.getStickerNumber() == 8)
					{
						makeTurn("R", cube, cs);
						makeTurn("U'", cube, cs);
						makeTurn("R'", cube, cs);
					}
				}
			}
			else
			{
				if (cs.getSurfaceNumber() == 2)
				{
					if (cs.getStickerNumber() == 0)
					{
						makeTurn("R'", cube, cs);
						makeTurn("U'", cube, cs);
						makeTurn("R", cube, cs);
					}
					else if (cs.getStickerNumber() == 2)
					{
						makeTurn("L", cube, cs);
						makeTurn("U", cube, cs);
						makeTurn("L'", cube, cs);
					}
					else if (cs.getStickerNumber() == 6)
					{
						makeTurn("R", cube, cs);
						makeTurn("U", cube, cs);
						makeTurn("R'", cube, cs);
					}
					else if (cs.getStickerNumber() == 8)
					{
						makeTurn("L'", cube, cs);
						makeTurn("U'", cube, cs);
						makeTurn("L", cube, cs);
					}
				}
				if (cs.getSurfaceNumber() == 5)
				{
					if (cs.getStickerNumber() == 6)
					{
						makeTurn("F'", cube, cs);
						makeTurn("U'", cube, cs);
						makeTurn("F", cube, cs);
					}
					else if (cs.getStickerNumber() == 0)
					{
						makeTurn("F", cube, cs);
						makeTurn("U", cube, cs);
						makeTurn("F'", cube, cs);
					}
				}
				if (cs.getSurfaceNumber() == 1)
				{
					if (cs.getStickerNumber() == 8)
					{
						makeTurn("R", cube, cs);
						makeTurn("U", cube, cs);
						makeTurn("R'", cube, cs);
					}
					else if (cs.getStickerNumber() == 2)
					{
						makeTurn("R'", cube, cs);
						makeTurn("U'", cube, cs);
						makeTurn("R", cube, cs);
					}
				}
				if (cs.getSurfaceNumber() == 3)
				{
					if (cs.getStickerNumber() == 0)
					{
						makeTurn("L", cube, cs);
						makeTurn("U", cube, cs);
						makeTurn("L'", cube, cs);
					}
					else if (cs.getStickerNumber() == 6)
					{
						makeTurn("L'", cube, cs);
						makeTurn("U'", cube, cs);
						makeTurn("L", cube, cs);
					}
				}
				if (cs.getSurfaceNumber() == 4)
				{
					if (cs.getStickerNumber() == 0)
					{
						makeTurn("B", cube, cs);
						makeTurn("U", cube, cs);
						makeTurn("B'", cube, cs);
					}
					else if (cs.getStickerNumber() == 6)
					{
						makeTurn("B'", cube, cs);
						makeTurn("U'", cube, cs);
						makeTurn("B", cube, cs);
					}
				}
			}
		}

		Timeout.timedOutIn(1000);

		//Edges
		while (cube.getUnsolvedF2LEdgeSticker() != null)
		{
			if (Timeout.timedOut()) return;

			cs = cube.getUnsolvedF2LEdgeSticker();

			if (cs.isOnUpperLayer())
			{
				//Bring piece in position
				for (int j = 0; j < 4; j++)
				{
					if (cube.F2LEdgeMatchesSurface(cs))
					{
						break;
					}
					makeTurn("U", cube, cs);
				}
				if (cs.getSurfaceNumber() == 5)
				{
					if (cube.getColorAt(cube.getOtherEdgeSticker(cs)) == "r")
						makeMultipleTurns("URU'R'U'F'UF", cube, cs);
					else if (cube.getColorAt(cube.getOtherEdgeSticker(cs)) == "o")
						makeMultipleTurns("U'L'ULUFU'F'", cube, cs);
				}
				else if (cs.getSurfaceNumber() == 4)
				{
					if (cube.getColorAt(cube.getOtherEdgeSticker(cs)) == "r")
						makeMultipleTurns("U'R'URUBU'B'", cube, cs);
					else if (cube.getColorAt(cube.getOtherEdgeSticker(cs)) == "o")
						makeMultipleTurns("ULU'L'U'B'UB", cube, cs);
				}
				else if (cs.getSurfaceNumber() == 3)
				{
					if (cube.getColorAt(cube.getOtherEdgeSticker(cs)) == "g")
						makeMultipleTurns("UFU'F'U'L'UL", cube, cs);
					else if (cube.getColorAt(cube.getOtherEdgeSticker(cs)) == "b")
						makeMultipleTurns("U'B'UBULU'L'", cube, cs);
				}
				else if (cs.getSurfaceNumber() == 1)
				{
					if (cube.getColorAt(cube.getOtherEdgeSticker(cs)) == "g")
						makeMultipleTurns("U'F'UFURU'R'", cube, cs);
					else if (cube.getColorAt(cube.getOtherEdgeSticker(cs)) == "b")
						makeMultipleTurns("UBU'B'U'R'UR", cube, cs);
				}
			}
			else
			{
				if (cs.isAt(5, 1) || cs.isAt(3, 7)) makeMultipleTurns("L'ULUFU'F'", cube, cs);
				else if (cs.isAt(5, 7) || cs.isAt(1, 7)) makeMultipleTurns("RU'R'U'F'UF", cube, cs);
				else if (cs.isAt(4, 1) || cs.isAt(1, 1)) makeMultipleTurns("R'URUBU'B'", cube, cs);
				else if (cs.isAt(4, 7) || cs.isAt(3, 1)) makeMultipleTurns("LU'L'U'B'UB", cube, cs);
			}
		}
	}

	/**
	 * @param cube
	 * @return Whether OLL has been solved.
	 */
	private static boolean solveOLL(Cube cube)
	{
		if (cube.ollIsSolved()) return true;

		ArrayList ollAlgorithms = new ArrayList();

		ollAlgorithms.add("LR'FL'RU2LR'FL'R"); //#28
		ollAlgorithms.add("RUR'U'LR'FRF'L'"); //#57
		ollAlgorithms.add("L'RBRBR'B'L2R2FRF'L'"); //#20
		ollAlgorithms.add("R2DR'U2RD'R'U2R'"); //#23
		ollAlgorithms.add("R'F'LFRF'L'F"); //#24
		ollAlgorithms.add("R'FRB'R'F'RB"); //#25
		ollAlgorithms.add("RUR'URU2R'"); //#27
		ollAlgorithms.add("RU2R'U'RU'R'"); //#26
		ollAlgorithms.add("RU2R2U'R2U'R2U2R"); //#22
		ollAlgorithms.add("RU2R'U'RUR'U'RU'R'"); //#21

		ollAlgorithms.add("BULU'L'B'U'FRUR'U'F'"); //#3
		ollAlgorithms.add("BULU'L'B'UFRUR'U'F'"); //#4
		ollAlgorithms.add("RUR'UR'FRF'U2R'FRF'"); //#17
		ollAlgorithms.add("L'RBRBR'B'LR2FRF'"); //#19
		ollAlgorithms.add("FRUR'UF'U2F'LFL'"); //#18
		ollAlgorithms.add("FRUR'U'BF'ULU'L'B'"); //#2
		ollAlgorithms.add("RU2R2FRF'U2R'FRF'"); //#1

		ollAlgorithms.add("RUR'U'R'FRF'"); //#33
		ollAlgorithms.add("FRUR'U'F'"); //#45

		ollAlgorithms.add("BULU'L'B'"); //#44
		ollAlgorithms.add("B'U'R'URB"); //#43
		ollAlgorithms.add("RUB'U'R'URBR'"); //#32
		ollAlgorithms.add("R'U'FURU'R'F'R"); //#31

		ollAlgorithms.add("RUR'URU'R'U'R'FRF'"); //#38
		ollAlgorithms.add("L'U'LU'L'ULULF'L'F"); //#36

		ollAlgorithms.add("LFR'FRF'R'FRF2L'"); //#54
		ollAlgorithms.add("L'B'RB'R'BRB'R'B2L"); //#53
		ollAlgorithms.add("RB'RBR2U2FR'F'R"); //#50
		ollAlgorithms.add("R'FR'F'R2U2B'RBR'"); //#49
		ollAlgorithms.add("FRUR'U'RUR'U'F'"); //#48
		ollAlgorithms.add("F'L'U'LUL'U'LUF"); //#47

		ollAlgorithms.add("LF'L'U'LUFU'L'"); //#39
		ollAlgorithms.add("R'FRUR'U'F'UR"); //#40

		ollAlgorithms.add("RUR2U'R'FRURU'F'"); //#34
		ollAlgorithms.add("R'U'R'FRF'UR"); //#46

		ollAlgorithms.add("L'B2RBR'BL"); //#5
		ollAlgorithms.add("LF2R'F'RF'L'"); //#6

		ollAlgorithms.add("LFR'FRF2L'"); //#7
		ollAlgorithms.add("L'RB2R'B'RB'R'B2RBLR'"); //#12
		ollAlgorithms.add("L'B'RB'R'B2L"); //#8
		ollAlgorithms.add("L'R2BR'BRB2R'BLR'"); //#11

		ollAlgorithms.add("FRU'R'U'RUR'F'"); //#37
		ollAlgorithms.add("RU2R2FRF'RU2R'"); //#35
		ollAlgorithms.add("RUR'UR'FRF'RU2R'"); //#10
		ollAlgorithms.add("RUR'U'R'FR2UR'U'F'"); //#9

		ollAlgorithms.add("BULU'L'ULU'L'B'"); //#51
		ollAlgorithms.add("RUR'URU'BU'B'R'"); //#52
		ollAlgorithms.add("BULU'L'B'FRUR'U'RUR'U'F'"); //#56
		ollAlgorithms.add("RU2R2U'RU'R'U2FRF'"); //#55

		ollAlgorithms.add("LF'L'U'LFL'F'UF"); //#13
		ollAlgorithms.add("LFL'RUR'U'LF'L'"); //#16
		ollAlgorithms.add("R'FRUR'F'RFU'F'"); //#14
		ollAlgorithms.add("R'F'RL'U'LUR'FR"); //#15

		ollAlgorithms.add("RU'R'U2RUBU'B'U'R'"); //#41
		ollAlgorithms.add("R2UR'B'RU'R2URBR'"); //#30
		ollAlgorithms.add("L'ULU2L'U'B'UBUL"); //#42
		ollAlgorithms.add("L2U'LBL'UL2U'L'B'L"); //#29

		for (int j = 0; j < 4; j++)
		{
			addMovesToSolution = false;
			for (int i = 0; i < ollAlgorithms.size(); i++)
			{
				makeMultipleTurns((String) ollAlgorithms.get(i), cube, null);
				if (cube.ollIsSolved())
				{
					solution += ollAlgorithms.get(i);
					addMovesToSolution = true;
					return true;
				}
				undoMultipleTurns((String) ollAlgorithms.get(i), cube, null);
			}

			addMovesToSolution = true;
			makeTurn("U", cube, null);
		}

		return false;
	}

	/**
	 * @param cube
	 * @return Whether PLL has been solved.
	 */
	private static boolean solvePLL(Cube cube)
	{
		if (cube.pllIsSolved()) return true;

		ArrayList pllAlgorithms = new ArrayList();

		pllAlgorithms.add("R'FR'B2RF'R'B2R2"); //Aa
		pllAlgorithms.add("LF'LB2L'FLB2L2"); //Ab
		pllAlgorithms.add("RB'R'FRBR'F'RBR'FRB'R'F'"); //E

		pllAlgorithms.add("R2L2D'L'RF2LR'D'L2R2"); //Ua
		pllAlgorithms.add("R2L2DL'RF2LR'DL2R2"); //Ua
		pllAlgorithms.add("R2L2DR2L2U2R2L2DR2L2"); //H
		pllAlgorithms.add("R2L2DR2L2UR2L2D2RL'B2R2L2F2R'L"); //Z

		pllAlgorithms.add("R'UL'U2RU'R'U2LR"); //Ja
		pllAlgorithms.add("RUR'F'RUR'U'R'FR2U'R'"); //Jb
		pllAlgorithms.add("RUR'U'R'FR2U'R'U'RUR'F'"); //T
		pllAlgorithms.add("R'U2RU2R'FRUR'U'R'F'R2"); //Rb
		pllAlgorithms.add("RUR'F'RU2R'U2R'FRURU2R'"); //Ra
		pllAlgorithms.add("R'U'F'RUR'U'R'FR2U'R'U'RUR'UR"); //F

		pllAlgorithms.add("R2DB'UB'U'BD'R2F'UF"); //Ga
		pllAlgorithms.add("R'U'RB2DL'ULU'LD'B2"); //Gb
		pllAlgorithms.add("R2D'FU'FUF'DR2BU'B'"); //Gc
		pllAlgorithms.add("RUR'F2D'LU'L'UL'DF2"); //Gd

		pllAlgorithms.add("R'UR'U'B'R'B2U'B'UB'RBR"); //V
		pllAlgorithms.add("RU'LU2R'URL'U'LU2R'UL'"); //Na
		pllAlgorithms.add("L'UR'U2LU'RL'UR'U2LU'R"); //Nb
		pllAlgorithms.add("FRU'R'U'RUR'F'RUR'U'R'FRF'"); //Y

		for (int j = 0; j < 4; j++)
		{
			addMovesToSolution = false;
			for (int i = 0; i < pllAlgorithms.size(); i++)
			{
				makeMultipleTurns((String) pllAlgorithms.get(i), cube, null);
				if (cube.pllIsSolved())
				{
					solution += pllAlgorithms.get(i);
					addMovesToSolution = true;
					return true;
				}
				undoMultipleTurns((String) pllAlgorithms.get(i), cube, null);
			}

			addMovesToSolution = true;
			makeTurn("U", cube, null);
		}

		return false;
	}

	private static void rotateLastLayer(Cube cube)
	{
		for (int i = 0; i < 4; i++)
		{
			if (cube.isSolved()) return;
			makeTurn("U", cube, null);
		}
	}

	private static void makeTurn(String turn, Cube cube, Sticker sticker)
	{
		//Separate e.g U2 -> U and U
		if (turn.equals("U2"))
		{
			cube.applyRotation("U");
			if (sticker != null) sticker.applyRotation("U");
			cube.applyRotation("U");
			if (sticker != null) sticker.applyRotation("U");
		}
		else if (turn.equals("D2"))
		{
			cube.applyRotation("D");
			if (sticker != null) sticker.applyRotation("D");
			cube.applyRotation("D");
			if (sticker != null) sticker.applyRotation("D");
		}
		else if (turn.equals("L2"))
		{
			cube.applyRotation("L");
			if (sticker != null) sticker.applyRotation("L");
			cube.applyRotation("L");
			if (sticker != null) sticker.applyRotation("L");
		}
		else if (turn.equals("R2"))
		{
			cube.applyRotation("R");
			if (sticker != null) sticker.applyRotation("R");
			cube.applyRotation("R");
			if (sticker != null) sticker.applyRotation("R");
		}
		else if (turn.equals("F2"))
		{
			cube.applyRotation("F");
			if (sticker != null) sticker.applyRotation("F");
			cube.applyRotation("F");
			if (sticker != null) sticker.applyRotation("F");
		}
		else if (turn.equals("B2"))
		{
			cube.applyRotation("B");
			if (sticker != null) sticker.applyRotation("B");
			cube.applyRotation("B");
			if (sticker != null) sticker.applyRotation("B");
		}
		else
		{
			cube.applyRotation(turn); //Method to apply rotation to the virtual cube
			if (sticker != null) sticker.applyRotation(turn); //Update the position of the sticker
		}

		if (addMovesToSolution) solution += turn;
	}

	private static void makeMultipleTurns(String turns, Cube cube, Sticker sticker)
	{
		ArrayList moves = SplitSolution.splitSolution(turns);

		for (int i = 0; i < moves.size(); i++)
		{
			makeTurn((String) moves.get(i), cube, sticker);
		}
	}

	private static void undoMultipleTurns(String turns, Cube cube, Sticker sticker)
	{
		ArrayList moves = SplitSolution.splitSolution(turns);

		for (int i = moves.size() - 1; i >= 0; i--)
		{
			//Invert notations
			if (moves.get(i).equals("U")) moves.set(i, "U'");
			else if (moves.get(i).equals("U'")) moves.set(i, "U");
			else if (moves.get(i).equals("D")) moves.set(i, "D'");
			else if (moves.get(i).equals("D'")) moves.set(i, "D");
			else if (moves.get(i).equals("L")) moves.set(i, "L'");
			else if (moves.get(i).equals("L'")) moves.set(i, "L");
			else if (moves.get(i).equals("R")) moves.set(i, "R'");
			else if (moves.get(i).equals("R'")) moves.set(i, "R");
			else if (moves.get(i).equals("F")) moves.set(i, "F'");
			else if (moves.get(i).equals("F'")) moves.set(i, "F");
			else if (moves.get(i).equals("B")) moves.set(i, "B'");
			else if (moves.get(i).equals("B'")) moves.set(i, "B");

			makeTurn((String) moves.get(i), cube, sticker);
		}
	}

	/**
	 * Changes UU to U2, UUU to U' and removes UUUU
	 * @return new Solution
	 */
	private static String shortenSolution()
	{
		ArrayList splittedSolution = SplitSolution.splitSolution(solution);
		String shortenedString = "";
		int upperLayerCounter = 0;

		for (int i = 0; i < splittedSolution.size() + 1; i++)
		{
			if (i < splittedSolution.size())
			{
				if (splittedSolution.get(i).equals("U")) upperLayerCounter++;
				else
				{
					i--;
					if (upperLayerCounter == 4) remove(splittedSolution, i - 3, i);
					else if (upperLayerCounter == 3)
					{
						splittedSolution.set(i, "U'");
						remove(splittedSolution, i - 2, i - 1);
					}
					else if (upperLayerCounter == 2)
					{
						splittedSolution.set(i, "U2");
						remove(splittedSolution, i - 1, i - 1);
					}
					upperLayerCounter = 0;
					i++;
				}
			}
			else
			{
				i--;
				if (upperLayerCounter == 4) remove(splittedSolution, i - 4, i);
				else if (upperLayerCounter == 3)
				{
					splittedSolution.set(i, "U'");
					remove(splittedSolution, i - 2, i - 1);
				}
				else if (upperLayerCounter == 2)
				{
					splittedSolution.set(i, "U2");
					remove(splittedSolution, i - 1, i - 1);
				}
				upperLayerCounter = 0;
				i++;
			}
		}

		for (int i = 0; i < splittedSolution.size(); i++)
		{
			shortenedString += splittedSolution.get(i);
		}

		return shortenedString;
	}

	private static void remove(ArrayList arrayList, int startIndex, int endIndex)
	{
		for (int i = 0; startIndex + i <= endIndex; i++)
		{
			arrayList.remove(startIndex);
		}
	}
}
