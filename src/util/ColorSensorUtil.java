package util;

import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import sensor.ColorSensor;

public class ColorSensorUtil
{
	private static SampleProvider colorProvider = ColorSensor.getColorSensor().getRGBMode();
	private static float[] colorSample = new float[3];

	/**
	 * Measure the intensity of Red, Green and Blue multiple times and returns
	 * the maximum, minimum and average values.
	 * 
	 * @param numberOfMeasurements
	 * @return Float array, [0] - [2]: smallest RGB Values, [3] - [5]: biggest
	 *         RGB Values, [6] - [8]: average RGB Values
	 */
	public static float[] getRGBRange(int numberOfMeasurements)
	{
		float[] minMaxAvr = new float[9];
		int currentNumberOfMeasurements = numberOfMeasurements;

		// First measurement
		currentNumberOfMeasurements--;
		colorProvider.fetchSample(colorSample, 0);
		for (int i = 0; i < minMaxAvr.length; i++)
		{
			if (i < 3) minMaxAvr[i] = colorSample[i];
			else if (i < 6) minMaxAvr[i] = colorSample[i - 3];
			else minMaxAvr[i] = colorSample[i - 6];
		}

		while (currentNumberOfMeasurements > 0)
		{
			Delay.msDelay(150);
			currentNumberOfMeasurements--;

			colorProvider.fetchSample(colorSample, 0);

			for (int i = 0; i < minMaxAvr.length; i++)
			{
				if (i < 3)
				{
					if (colorSample[i] < minMaxAvr[i]) minMaxAvr[i] = colorSample[i];
				}
				else if (i < 6)
				{
					if (colorSample[i - 3] > minMaxAvr[i]) minMaxAvr[i] = colorSample[i - 3];
				}
				else
				{
					minMaxAvr[i] += colorSample[i - 6];
				}
			}
		}

		for (int i = 6; i < minMaxAvr.length; i++)
		{
			minMaxAvr[i] = minMaxAvr[i] / numberOfMeasurements;
		}

		return minMaxAvr;
	}

	public static void printGetRGBRange(int numberOfMeasurements)
	{
		float[] minMaxAvrRGB = getRGBRange(numberOfMeasurements);

		System.out.println("Min:");
		System.out.println(minMaxAvrRGB[0]);
		System.out.println(minMaxAvrRGB[1]);
		System.out.println(minMaxAvrRGB[2]);
		System.out.println("Max:");
		System.out.println(minMaxAvrRGB[3]);
		System.out.println(minMaxAvrRGB[4]);
		System.out.println(minMaxAvrRGB[5]);
		System.out.println("Average:");
		System.out.println(minMaxAvrRGB[6]);
		System.out.println(minMaxAvrRGB[7]);
		System.out.println(minMaxAvrRGB[8]);
	}
}
