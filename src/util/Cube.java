package util;

public class Cube
{
	/**
	 * Surface:
	 * 0 1 2
	 * 3 4 5
	 * 6 7 8
	 */
	private String[][] stickers; //stickers[surface 0 - 5][sticker 0 - 8]

	public Cube(String[][] stickers)
	{
		this.stickers = stickers;
	}

	/**
	 * Set the specified sticker to the color. ("b", "g", "o", "r", "w" or "y")
	 * @param surfaceNumber
	 * @param stickerNumber
	 * @param color
	 */
	public void setSticker(int surfaceNumber, int stickerNumber, String color)
	{
		stickers[surfaceNumber][stickerNumber] = color;
	}

	/**
	 * @return stickers[surface 0 - 5][sticker 0 - 8]
	 */
	public String[][] getStickers()
	{
		return stickers;
	}

	/**
	 * Surface: <br>
	 * 0 1 2	<br>
	 * 3 4 5	<br>
	 * 6 7 8	<br>
	 */
	public String[] getSurface(int surface)
	{
		return stickers[surface];
	}

	/**
	 * Return the sticker color as string.
	 * @param sticker
	 * @return "b", "g", "o", "r", "w" or "y"
	 */
	public String getColorAt(Sticker sticker)
	{
		return stickers[sticker.getSurfaceNumber()][sticker.getStickerNumber()];
	}

	/**
	 * Apply rotation to the virtual cube.
	 * @param notation
	 */
	public void applyRotation(String notation)
	{
		if (notation.equals("U2"))
		{
			notation = "U";
			applyRotation("U");
		}
		else if (notation.equals("D2"))
		{
			notation = "D";
			applyRotation("D");
		}
		else if (notation.equals("F2"))
		{
			notation = "F";
			applyRotation("F");
		}
		else if (notation.equals("B2"))
		{
			notation = "B";
			applyRotation("B");
		}
		else if (notation.equals("L2"))
		{
			notation = "L";
			applyRotation("L");
		}
		else if (notation.equals("R2"))
		{
			notation = "R";
			applyRotation("R");
		}

		//Up
		if (notation.equals("U") || notation.equals("U'"))
		{
			String s5st2 = stickers[5][2]; //Front surface
			String s5st5 = stickers[5][5];
			String s5st8 = stickers[5][8];
			String s1st6 = stickers[1][6]; //Right surface
			String s1st3 = stickers[1][3];
			String s1st0 = stickers[1][0];
			String s4st2 = stickers[4][2]; //Back surface
			String s4st5 = stickers[4][5];
			String s4st8 = stickers[4][8];
			String s3st2 = stickers[3][2]; //Left surface
			String s3st5 = stickers[3][5];
			String s3st8 = stickers[3][8];

			if (notation.equals("U"))
			{
				applySurfaceRotation(0, true);

				stickers[5][2] = s1st6;
				stickers[5][5] = s1st3;
				stickers[5][8] = s1st0;
				stickers[1][6] = s4st2;
				stickers[1][3] = s4st5;
				stickers[1][0] = s4st8;
				stickers[4][2] = s3st2;
				stickers[4][5] = s3st5;
				stickers[4][8] = s3st8;
				stickers[3][2] = s5st2;
				stickers[3][5] = s5st5;
				stickers[3][8] = s5st8;
			}
			else
			{
				applySurfaceRotation(0, false);

				stickers[5][2] = s3st2;
				stickers[5][5] = s3st5;
				stickers[5][8] = s3st8;
				stickers[1][6] = s5st2;
				stickers[1][3] = s5st5;
				stickers[1][0] = s5st8;
				stickers[4][2] = s1st6;
				stickers[4][5] = s1st3;
				stickers[4][8] = s1st0;
				stickers[3][2] = s4st2;
				stickers[3][5] = s4st5;
				stickers[3][8] = s4st8;
			}
		}
		//Down
		else if (notation.equals("D") || notation.equals("D'"))
		{
			String s5st0 = stickers[5][0]; //Front surface
			String s5st3 = stickers[5][3];
			String s5st6 = stickers[5][6];
			String s1st8 = stickers[1][8]; //Right surface
			String s1st5 = stickers[1][5];
			String s1st2 = stickers[1][2];
			String s4st0 = stickers[4][0]; //Back surface
			String s4st3 = stickers[4][3];
			String s4st6 = stickers[4][6];
			String s3st0 = stickers[3][0]; //Left surface
			String s3st3 = stickers[3][3];
			String s3st6 = stickers[3][6];

			if (notation.equals("D"))
			{
				applySurfaceRotation(2, true);

				stickers[5][0] = s3st0;
				stickers[5][3] = s3st3;
				stickers[5][6] = s3st6;
				stickers[1][8] = s5st0;
				stickers[1][5] = s5st3;
				stickers[1][2] = s5st6;
				stickers[4][0] = s1st8;
				stickers[4][3] = s1st5;
				stickers[4][6] = s1st2;
				stickers[3][0] = s4st0;
				stickers[3][3] = s4st3;
				stickers[3][6] = s4st6;
			}
			else
			{
				applySurfaceRotation(2, false);

				stickers[5][0] = s1st8;
				stickers[5][3] = s1st5;
				stickers[5][6] = s1st2;
				stickers[1][8] = s4st0;
				stickers[1][5] = s4st3;
				stickers[1][2] = s4st6;
				stickers[4][0] = s3st0;
				stickers[4][3] = s3st3;
				stickers[4][6] = s3st6;
				stickers[3][0] = s5st0;
				stickers[3][3] = s5st3;
				stickers[3][6] = s5st6;
			}
		}
		//Front
		else if (notation.equals("F") || notation.equals("F'"))
		{
			String s0st6 = stickers[0][6]; //Up
			String s0st7 = stickers[0][7];
			String s0st8 = stickers[0][8];
			String s1st6 = stickers[1][6]; //Right
			String s1st7 = stickers[1][7];
			String s1st8 = stickers[1][8];
			String s2st6 = stickers[2][6]; //Down
			String s2st7 = stickers[2][7];
			String s2st8 = stickers[2][8];
			String s3st6 = stickers[3][6]; //Left
			String s3st7 = stickers[3][7];
			String s3st8 = stickers[3][8];

			if (notation.equals("F"))
			{
				applySurfaceRotation(5, true);

				stickers[0][6] = s3st6;
				stickers[0][7] = s3st7;
				stickers[0][8] = s3st8;
				stickers[1][6] = s0st6;
				stickers[1][7] = s0st7;
				stickers[1][8] = s0st8;
				stickers[2][6] = s1st6;
				stickers[2][7] = s1st7;
				stickers[2][8] = s1st8;
				stickers[3][6] = s2st6;
				stickers[3][7] = s2st7;
				stickers[3][8] = s2st8;
			}
			else
			{
				applySurfaceRotation(5, false);

				stickers[0][6] = s1st6;
				stickers[0][7] = s1st7;
				stickers[0][8] = s1st8;
				stickers[1][6] = s2st6;
				stickers[1][7] = s2st7;
				stickers[1][8] = s2st8;
				stickers[2][6] = s3st6;
				stickers[2][7] = s3st7;
				stickers[2][8] = s3st8;
				stickers[3][6] = s0st6;
				stickers[3][7] = s0st7;
				stickers[3][8] = s0st8;
			}
		}
		//Back
		else if (notation.equals("B") || notation.equals("B'"))
		{
			String s0st0 = stickers[0][0]; //Up
			String s0st1 = stickers[0][1];
			String s0st2 = stickers[0][2];
			String s1st0 = stickers[1][0]; //Right
			String s1st1 = stickers[1][1];
			String s1st2 = stickers[1][2];
			String s2st0 = stickers[2][0]; //Down
			String s2st1 = stickers[2][1];
			String s2st2 = stickers[2][2];
			String s3st0 = stickers[3][0]; //Left
			String s3st1 = stickers[3][1];
			String s3st2 = stickers[3][2];

			if (notation.equals("B"))
			{
				applySurfaceRotation(4, true);

				stickers[0][0] = s1st0;
				stickers[0][1] = s1st1;
				stickers[0][2] = s1st2;
				stickers[1][0] = s2st0;
				stickers[1][1] = s2st1;
				stickers[1][2] = s2st2;
				stickers[2][0] = s3st0;
				stickers[2][1] = s3st1;
				stickers[2][2] = s3st2;
				stickers[3][0] = s0st0;
				stickers[3][1] = s0st1;
				stickers[3][2] = s0st2;
			}
			else
			{
				applySurfaceRotation(4, false);

				stickers[0][0] = s3st0;
				stickers[0][1] = s3st1;
				stickers[0][2] = s3st2;
				stickers[1][0] = s0st0;
				stickers[1][1] = s0st1;
				stickers[1][2] = s0st2;
				stickers[2][0] = s1st0;
				stickers[2][1] = s1st1;
				stickers[2][2] = s1st2;
				stickers[3][0] = s2st0;
				stickers[3][1] = s2st1;
				stickers[3][2] = s2st2;
			}
		}
		//Left
		else if (notation.equals("L") || notation.equals("L'"))
		{
			String s0st0 = stickers[0][0]; //Up
			String s0st3 = stickers[0][3];
			String s0st6 = stickers[0][6];
			String s5st2 = stickers[5][2]; //Front
			String s5st1 = stickers[5][1];
			String s5st0 = stickers[5][0];
			String s2st8 = stickers[2][8]; //Down
			String s2st5 = stickers[2][5];
			String s2st2 = stickers[2][2];
			String s4st6 = stickers[4][6]; //Back
			String s4st7 = stickers[4][7];
			String s4st8 = stickers[4][8];

			if (notation.equals("L"))
			{
				applySurfaceRotation(3, true);

				stickers[0][0] = s4st6;
				stickers[0][3] = s4st7;
				stickers[0][6] = s4st8;
				stickers[5][2] = s0st0;
				stickers[5][1] = s0st3;
				stickers[5][0] = s0st6;
				stickers[2][8] = s5st2;
				stickers[2][5] = s5st1;
				stickers[2][2] = s5st0;
				stickers[4][6] = s2st8;
				stickers[4][7] = s2st5;
				stickers[4][8] = s2st2;
			}
			else
			{
				applySurfaceRotation(3, false);

				stickers[0][0] = s5st2;
				stickers[0][3] = s5st1;
				stickers[0][6] = s5st0;
				stickers[5][2] = s2st8;
				stickers[5][1] = s2st5;
				stickers[5][0] = s2st2;
				stickers[2][8] = s4st6;
				stickers[2][5] = s4st7;
				stickers[2][2] = s4st8;
				stickers[4][6] = s0st0;
				stickers[4][7] = s0st3;
				stickers[4][8] = s0st6;
			}
		}
		//Right
		else if (notation.equals("R") || notation.equals("R'"))
		{
			String s0st2 = stickers[0][2]; //Up
			String s0st5 = stickers[0][5];
			String s0st8 = stickers[0][8];
			String s5st8 = stickers[5][8]; //Front
			String s5st7 = stickers[5][7];
			String s5st6 = stickers[5][6];
			String s2st6 = stickers[2][6]; //Down
			String s2st3 = stickers[2][3];
			String s2st0 = stickers[2][0];
			String s4st0 = stickers[4][0]; //Back
			String s4st1 = stickers[4][1];
			String s4st2 = stickers[4][2];

			if (notation.equals("R"))
			{
				applySurfaceRotation(1, true);

				stickers[0][2] = s5st8;
				stickers[0][5] = s5st7;
				stickers[0][8] = s5st6;
				stickers[5][8] = s2st6;
				stickers[5][7] = s2st3;
				stickers[5][6] = s2st0;
				stickers[2][6] = s4st0;
				stickers[2][3] = s4st1;
				stickers[2][0] = s4st2;
				stickers[4][0] = s0st2;
				stickers[4][1] = s0st5;
				stickers[4][2] = s0st8;
			}
			else
			{
				applySurfaceRotation(1, false);

				stickers[0][2] = s4st0;
				stickers[0][5] = s4st1;
				stickers[0][8] = s4st2;
				stickers[5][8] = s0st2;
				stickers[5][7] = s0st5;
				stickers[5][6] = s0st8;
				stickers[2][6] = s5st8;
				stickers[2][3] = s5st7;
				stickers[2][0] = s5st6;
				stickers[4][0] = s2st6;
				stickers[4][1] = s2st3;
				stickers[4][2] = s2st0;
			}
		}
	}

	private void applySurfaceRotation(int surface, boolean clockwise)
	{
		String s0 = stickers[surface][0]; //String[] face = stickers[surface]; doesn't work
		String s1 = stickers[surface][1];
		String s2 = stickers[surface][2];
		String s3 = stickers[surface][3];
		String s5 = stickers[surface][5];
		String s6 = stickers[surface][6];
		String s7 = stickers[surface][7];
		String s8 = stickers[surface][8];

		if (clockwise)
		{
			stickers[surface][0] = s6;
			stickers[surface][1] = s3;
			stickers[surface][2] = s0;
			stickers[surface][3] = s7;
			stickers[surface][5] = s1;
			stickers[surface][6] = s8;
			stickers[surface][7] = s5;
			stickers[surface][8] = s2;
		}
		else
		{
			stickers[surface][0] = s2;
			stickers[surface][1] = s5;
			stickers[surface][2] = s8;
			stickers[surface][3] = s1;
			stickers[surface][5] = s7;
			stickers[surface][6] = s0;
			stickers[surface][7] = s3;
			stickers[surface][8] = s6;
		}
	}

	/**
	 * Get the other sticker of an edge.
	 * @param sticker
	 * @return null if the given sticker doesn't belong to an edge
	 */
	public Sticker getOtherEdgeSticker(Sticker sticker)
	{
		if (!sticker.onEdgePiece()) return null;

		int surfaceNumber = sticker.getSurfaceNumber();
		int stickerNumber = sticker.getStickerNumber();

		if (sticker.getSurfaceNumber() >= 0 && sticker.getSurfaceNumber() < 4)
		{
			if (sticker.getStickerNumber() == 3)
			{
				surfaceNumber--;
				stickerNumber = 5;
			}
			if (sticker.getStickerNumber() == 5)
			{
				surfaceNumber++;
				stickerNumber = 3;
			}
			if (surfaceNumber < 0) surfaceNumber += 4;
			if (surfaceNumber > 3) surfaceNumber -= 4;

			if (sticker.getStickerNumber() == 1)
			{
				surfaceNumber = 4;

				if (sticker.getSurfaceNumber() == 0) stickerNumber = 5;
				else if (sticker.getSurfaceNumber() == 1) stickerNumber = 1;
				else if (sticker.getSurfaceNumber() == 2) stickerNumber = 3;
				else if (sticker.getSurfaceNumber() == 3) stickerNumber = 7;
			}
			if (sticker.getStickerNumber() == 7)
			{
				surfaceNumber = 5;

				if (sticker.getSurfaceNumber() == 0) stickerNumber = 5;
				else if (sticker.getSurfaceNumber() == 1) stickerNumber = 7;
				else if (sticker.getSurfaceNumber() == 2) stickerNumber = 3;
				else if (sticker.getSurfaceNumber() == 3) stickerNumber = 1;
			}
		}
		else if (sticker.getSurfaceNumber() == 4)
		{
			stickerNumber = 1;

			if (sticker.getStickerNumber() == 1) surfaceNumber = 1;
			else if (sticker.getStickerNumber() == 3) surfaceNumber = 2;
			else if (sticker.getStickerNumber() == 5) surfaceNumber = 0;
			else if (sticker.getStickerNumber() == 7) surfaceNumber = 3;
		}
		else if (sticker.getSurfaceNumber() == 5)
		{
			stickerNumber = 7;

			if (sticker.getStickerNumber() == 1) surfaceNumber = 3;
			else if (sticker.getStickerNumber() == 3) surfaceNumber = 2;
			else if (sticker.getStickerNumber() == 5) surfaceNumber = 0;
			else if (sticker.getStickerNumber() == 7) surfaceNumber = 1;
		}

		return new Sticker(surfaceNumber, stickerNumber);
	}

	/**
	 * Gets an unsolved yellow edge sticker. Use this to solve the cross.
	 * @return null if the cross is solved
	 */
	public Sticker getUnsolvedCrossSticker()
	{
		//White surface
		for (int i = 0; i < 9; i++)
		{
			Sticker currentSticker = new Sticker(0, i);
			if (!currentSticker.onEdgePiece()) continue;

			if (getColorAt(currentSticker) == "y") return currentSticker;
		}

		//Yellow surface
		for (int i = 0; i < 9; i++)
		{
			Sticker currentSticker = new Sticker(2, i);
			if (!currentSticker.onEdgePiece()) continue;

			if (getColorAt(currentSticker) == "y" && !crossStickerMatchesSurface(currentSticker)) return currentSticker;
		}

		//Other surfaces
		for (int i = 1; i < 6; i++)
		{
			if (i == 2) continue;
			for (int j = 0; j < 9; j++)
			{
				Sticker currentSticker = new Sticker(i, j);
				if (!currentSticker.onEdgePiece()) continue;

				if (getColorAt(currentSticker) == "y") return currentSticker;
			}
		}

		return null;
	}

	/**
	 * Gets an unsolved yellow corner sticker. Use this to solve the F2L corners.
	 * @return null if all four corner stickers are solved
	 */
	public Sticker getUnsolvedF2LCornerSticker()
	{
		//Upper layer, not on top
		if (getColorAt(new Sticker(1, 0)) == "y") return new Sticker(1, 0);
		if (getColorAt(new Sticker(1, 6)) == "y") return new Sticker(1, 6);
		if (getColorAt(new Sticker(3, 2)) == "y") return new Sticker(3, 2);
		if (getColorAt(new Sticker(3, 8)) == "y") return new Sticker(3, 8);
		if (getColorAt(new Sticker(4, 2)) == "y") return new Sticker(4, 2);
		if (getColorAt(new Sticker(4, 8)) == "y") return new Sticker(4, 8);
		if (getColorAt(new Sticker(5, 2)) == "y") return new Sticker(5, 2);
		if (getColorAt(new Sticker(5, 8)) == "y") return new Sticker(5, 8);

		//Stickers on top
		for (int i = 0; i < 9; i++)
		{
			Sticker currentSticker = new Sticker(0, i);
			if (!currentSticker.onCornerPiece()) continue;

			if (getColorAt(currentSticker) == "y") return currentSticker;
		}

		//Sticker on the bottom side
		for (int i = 0; i < 9; i++)
		{
			Sticker currentSticker = new Sticker(2, i);
			if (!currentSticker.onCornerPiece()) continue;

			if (getColorAt(currentSticker) == "y" && !cornerStickerMatchesSurface(currentSticker))
				return currentSticker;
		}

		//Bottom layer, without the stickers on the bottom
		if (getColorAt(new Sticker(1, 2)) == "y") return new Sticker(1, 2);
		if (getColorAt(new Sticker(1, 8)) == "y") return new Sticker(1, 8);
		if (getColorAt(new Sticker(3, 0)) == "y") return new Sticker(3, 0);
		if (getColorAt(new Sticker(3, 6)) == "y") return new Sticker(3, 6);
		if (getColorAt(new Sticker(4, 0)) == "y") return new Sticker(4, 0);
		if (getColorAt(new Sticker(4, 6)) == "y") return new Sticker(4, 6);
		if (getColorAt(new Sticker(5, 0)) == "y") return new Sticker(5, 0);
		if (getColorAt(new Sticker(5, 6)) == "y") return new Sticker(5, 6);

		return null;
	}

	public Sticker getUnsolvedF2LEdgeSticker()
	{
		//Stickers on upper layer, not on top
		for (int i = 1; i < 6; i++)
		{
			if (i == 2) continue;

			for (int j = 0; j < 9; j++)
			{
				Sticker currentSticker = new Sticker(i, j);

				if (!currentSticker.onEdgePiece()) continue;
				if (!currentSticker.isOnUpperLayer()) continue;
				if (isF2LEdgeSticker(currentSticker)) return currentSticker;
			}
		}

		//Stickers on middle layer
		for (int i = 1; i < 6; i++)
		{
			if (i == 2) continue;

			for (int j = 0; j < 9; j++)
			{
				Sticker currentSticker = new Sticker(i, j);

				if (!currentSticker.onEdgePiece()) continue;
				if (!currentSticker.isOnMiddleLayer() || currentSticker.onCenterPiece()) continue;
				if (isF2LEdgeSticker(currentSticker) && !stickerIsInF2LPos(currentSticker)) return currentSticker;
			}
		}
		return null;
	}

	public boolean ollIsSolved()
	{
		//Top completely white
		for (int i = 0; i < 9; i++)
		{
			Sticker currentSticker = new Sticker(0, i);
			if (getColorAt(currentSticker) != "w") return false;
		}
		//Bottom completely yellow
		for (int i = 0; i < 9; i++)
		{
			Sticker currentSticker = new Sticker(2, i);
			if (getColorAt(currentSticker) != "y") return false;
		}
		//Others, first two layers solved
		for (int i = 0; i < 9; i++)
		{
			Sticker currentSticker = new Sticker(1, i);
			if (currentSticker.isOnUpperLayer()) continue;
			if (getColorAt(currentSticker) != "r") return false;
		}
		for (int i = 0; i < 9; i++)
		{
			Sticker currentSticker = new Sticker(3, i);
			if (currentSticker.isOnUpperLayer()) continue;
			if (getColorAt(currentSticker) != "o") return false;
		}
		for (int i = 0; i < 9; i++)
		{
			Sticker currentSticker = new Sticker(4, i);
			if (currentSticker.isOnUpperLayer()) continue;
			if (getColorAt(currentSticker) != "b") return false;
		}
		for (int i = 0; i < 9; i++)
		{
			Sticker currentSticker = new Sticker(5, i);
			if (currentSticker.isOnUpperLayer()) continue;
			if (getColorAt(currentSticker) != "g") return false;
		}

		return true;
	}

	public boolean pllIsSolved()
	{
		for (int i = 0; i < 4; i++)
		{
			if (this.isSolved())
			{
				for (int j = 0; j < i; j++)
				{
					this.applyRotation("U'"); //Undo moves before returning
				}
				return true;
			}
			this.applyRotation("U");
		}

		return false;
	}

	public boolean isSolved()
	{
		String[] colors = { "w", "r", "y", "o", "b", "g" };

		for (int i = 0; i < 6; i++)
		{
			for (int j = 0; j < 9; j++)
			{
				Sticker currentSticker = new Sticker(i, j);
				if (getColorAt(currentSticker) != colors[i]) return false;
			}
		}

		return true;
	}

	private boolean isF2LEdgeSticker(Sticker sticker)
	{
		//None of the two stickers on the piece is yellow or white -> return true
		Sticker otherSticker = getOtherEdgeSticker(sticker);
		return (getColorAt(sticker) != "y" && getColorAt(sticker) != "w" && getColorAt(otherSticker) != "y"
				&& getColorAt(otherSticker) != "w");
	}

	private boolean stickerIsInF2LPos(Sticker sticker)
	{
		Sticker otherSticker = getOtherEdgeSticker(sticker);

		if (sticker.isAt(1, 1)) return getColorAt(sticker) == "r" && getColorAt(otherSticker) == "b";
		if (sticker.isAt(1, 7)) return getColorAt(sticker) == "r" && getColorAt(otherSticker) == "g";
		if (sticker.isAt(3, 1)) return getColorAt(sticker) == "o" && getColorAt(otherSticker) == "b";
		if (sticker.isAt(3, 7)) return getColorAt(sticker) == "o" && getColorAt(otherSticker) == "g";
		if (sticker.isAt(4, 1)) return getColorAt(sticker) == "b" && getColorAt(otherSticker) == "r";
		if (sticker.isAt(4, 7)) return getColorAt(sticker) == "b" && getColorAt(otherSticker) == "o";
		if (sticker.isAt(5, 1)) return getColorAt(sticker) == "g" && getColorAt(otherSticker) == "o";
		if (sticker.isAt(5, 7)) return getColorAt(sticker) == "g" && getColorAt(otherSticker) == "r";

		return false;
	}

	public boolean crossStickerMatchesSurface(Sticker sticker)
	{
		//Stickers on the bottom side
		if (sticker.getSurfaceNumber() == 2)
		{
			if (sticker.getStickerNumber() == 1) return this.getColorAt(this.getOtherEdgeSticker(sticker)) == "b";
			else if (sticker.getStickerNumber() == 3) return this.getColorAt(this.getOtherEdgeSticker(sticker)) == "r";
			else if (sticker.getStickerNumber() == 5) return this.getColorAt(this.getOtherEdgeSticker(sticker)) == "o";
			else if (sticker.getStickerNumber() == 7) return this.getColorAt(this.getOtherEdgeSticker(sticker)) == "g";
		}
		//Stickers on the top side
		else if (sticker.getSurfaceNumber() == 0)
		{
			if (sticker.getStickerNumber() == 1) return this.getColorAt(this.getOtherEdgeSticker(sticker)) == "b";
			else if (sticker.getStickerNumber() == 3) return this.getColorAt(this.getOtherEdgeSticker(sticker)) == "o";
			else if (sticker.getStickerNumber() == 5) return this.getColorAt(this.getOtherEdgeSticker(sticker)) == "r";
			else if (sticker.getStickerNumber() == 7) return this.getColorAt(this.getOtherEdgeSticker(sticker)) == "g";
		}
		return true;
	}

	public boolean F2LEdgeMatchesSurface(Sticker sticker)
	{
		if (sticker.isAt(5, 5)) return getColorAt(sticker) == "g";
		if (sticker.isAt(1, 3)) return getColorAt(sticker) == "r";
		if (sticker.isAt(3, 5)) return getColorAt(sticker) == "o";
		if (sticker.isAt(4, 5)) return getColorAt(sticker) == "b";

		return true;
	}

	public boolean cornerStickerMatchesSurface(Sticker sticker)
	{
		//Upper layer, without stickers on top
		if (sticker.isAt(1, 0)) return (getColorAt(new Sticker(0, 2)) == "r") && (getColorAt(new Sticker(4, 2)) == "b");
		if (sticker.isAt(1, 6)) return (getColorAt(new Sticker(0, 8)) == "r") && (getColorAt(new Sticker(5, 8)) == "g");
		if (sticker.isAt(3, 2)) return (getColorAt(new Sticker(0, 0)) == "o") && (getColorAt(new Sticker(4, 8)) == "b");
		if (sticker.isAt(3, 8)) return (getColorAt(new Sticker(0, 6)) == "o") && (getColorAt(new Sticker(5, 2)) == "g");
		if (sticker.isAt(4, 2)) return (getColorAt(new Sticker(0, 2)) == "b") && (getColorAt(new Sticker(1, 0)) == "r");
		if (sticker.isAt(4, 8)) return (getColorAt(new Sticker(0, 0)) == "b") && (getColorAt(new Sticker(3, 2)) == "o");
		if (sticker.isAt(5, 2)) return (getColorAt(new Sticker(0, 6)) == "g") && (getColorAt(new Sticker(3, 8)) == "o");
		if (sticker.isAt(5, 8)) return (getColorAt(new Sticker(0, 8)) == "g") && (getColorAt(new Sticker(1, 6)) == "r");

		//Stickers on top
		if (sticker.isAt(0, 0)) return (getColorAt(new Sticker(3, 2)) == "b") && (getColorAt(new Sticker(4, 8)) == "o");
		if (sticker.isAt(0, 2)) return (getColorAt(new Sticker(1, 0)) == "b") && (getColorAt(new Sticker(4, 2)) == "r");
		if (sticker.isAt(0, 6)) return (getColorAt(new Sticker(3, 8)) == "g") && (getColorAt(new Sticker(5, 2)) == "o");
		if (sticker.isAt(0, 8)) return (getColorAt(new Sticker(1, 6)) == "g") && (getColorAt(new Sticker(5, 8)) == "r");

		//Stickers on the bottom side
		if (sticker.isAt(2, 0)) return (getColorAt(new Sticker(1, 2)) == "r") && (getColorAt(new Sticker(4, 0)) == "b");
		if (sticker.isAt(2, 2)) return (getColorAt(new Sticker(3, 0)) == "o") && (getColorAt(new Sticker(4, 6)) == "b");
		if (sticker.isAt(2, 6)) return (getColorAt(new Sticker(1, 8)) == "r") && (getColorAt(new Sticker(5, 6)) == "g");
		if (sticker.isAt(2, 8)) return (getColorAt(new Sticker(3, 6)) == "o") && (getColorAt(new Sticker(5, 0)) == "g");

		//TODO

		return true;
	}
}
