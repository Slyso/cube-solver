package util;

import lejos.hardware.Button;

public class EditCube
{
	/**
	 * Allows the user to edit the cube by using the right, left and enter button.
	 * Returns as soon as the escape button is pressed.
	 * @param cube
	 * @return The edited cube
	 */
	public static Cube editCube(Cube cube)
	{
		int iteration = 0;
		int currentColor = 0;
		int selectedSurface = 0;
		int selectedSticker = 0;
		String[] colors = { "b", "g", "o", "r", "w", "y" };

		while (Button.ESCAPE.isUp())
		{
			PrintString.printCubeEditMode(cube, selectedSurface, selectedSticker, iteration); //Has a delay

			if (Button.RIGHT.isDown())
			{
				selectedSticker++;
				if (selectedSticker > 8)
				{
					selectedSticker = 0;
					if (selectedSurface < 5) selectedSurface++;
					else selectedSurface = 0;
				}
			}
			if (Button.LEFT.isDown())
			{
				selectedSticker--;
				if (selectedSticker < 0)
				{
					selectedSticker = 8;
					if (selectedSurface > 0) selectedSurface--;
					else selectedSurface = 5;
				}
			}
			if (Button.ENTER.isDown())
			{
				cube.setSticker(selectedSurface, selectedSticker, colors[currentColor]);
				if (currentColor < 5) currentColor++;
				else currentColor = 0;
			}

			iteration++;
		}
		return cube;
	}
}
