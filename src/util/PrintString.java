package util;

import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.utility.Delay;

public class PrintString
{
	private static GraphicsLCD g = BrickFinder.getDefault().getGraphicsLCD();

	/**
	 * Display a string
	 * @param string
	 * @param wait Wait until the escape button is pressed if true
	 */
	public static void print(String string, boolean wait)
	{
		g.clear();
		int y = 10;

		String originalString = string;

		//If the string is too long to be displayed in one line
		while (string.length() > 16)
		{
			print(string.substring(0, 16), y);
			string = string.substring(16);
			y += 20;
		}
		print(string, y);

		if (!wait) return;

		while (Button.ESCAPE.isUp())
		{
			if (Button.DOWN.isDown())
			{
				if (originalString.length() < 16) return;
				Delay.msDelay(500);
				//Scroll down
				print(originalString.substring(16), true);
				return;
			}
			Delay.msDelay(50);
		}
		Delay.msDelay(500);
	}

	private static void print(String string, int y)
	{
		g.drawString(string, 5, y, 0);
	}

	/**
	 * Display a string array until the escape button is pressed
	 * @param string
	 */
	public static void print(String[] string)
	{
		g.clear();
		for (int i = 0; i < string.length; i++)
		{
			g.drawString(string[i], 10, 12 * i, 0);
		}

		while (Button.ESCAPE.isUp())
		{
			Delay.msDelay(50);
		}
		Delay.msDelay(500);
	}

	/**
	 * Display a string[][] until the escape button is pressed
	 * @param string
	 */
	public static void print(String[][] string)
	{
		g.clear();
		for (int x = 0; x < string.length; x++)
		{
			for (int y = 0; y < string[x].length; y++)
			{
				g.drawString(string[x][y], 10 * x, 12 * y, 0);
			}
		}

		while (Button.ESCAPE.isUp())
		{
			Delay.msDelay(50);
		}
		Delay.msDelay(500);
	}

	/**
	 * Display every surface of the cube until the escape button is pressed
	 * @param cube
	 */
	public static void printCube(Cube cube)
	{
		g.clear();
		String[][] stickers = cube.getStickers();
		for (int i = 0; i < stickers.length; i++)
		{
			if (i < 4) printSurface(stickers[i], i * 42, 0);
			else if (i == 4) printSurface(stickers[i], 3 * 42, -40);
			else printSurface(stickers[i], 3 * 42, 40);
		}

		while (Button.ESCAPE.isUp())
		{
			Delay.msDelay(50);
		}
		Delay.msDelay(500);
	}

	public static void printCubeEditMode(Cube cube, int selectedSurface, int selectedSticker, int iteration)
	{
		g.clear();
		String originalValue = cube.getStickers()[selectedSurface][selectedSticker];
		if (iteration % 3 == 0) cube.setSticker(selectedSurface, selectedSticker, "");
		String[][] stickers = cube.getStickers();
		for (int i = 0; i < stickers.length; i++)
		{
			if (i < 4) printSurface(stickers[i], i * 42, 0);
			else if (i == 4) printSurface(stickers[i], 3 * 42, -40);
			else printSurface(stickers[i], 3 * 42, 40);
		}

		Delay.msDelay(400);
		cube.setSticker(selectedSurface, selectedSticker, originalValue);
	}

	private static void printSurface(String[] string, int xOffset, int yOffset)
	{
		for (int x = 0; x < 3; x++)
		{
			for (int y = 0; y < 3; y++)
			{
				g.drawString(string[x + y * 3], 12 * x + xOffset, 12 * y + yOffset + 45, 0);
			}
		}
	}
}
