package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadColorValues
{
	/**
	 * Try to read the file values.txt
	 * @return The RGB values or null if the file doesn't exist
	 */
	public static float[][] readFile()
	{
		Scanner scanner;
		float[][] colors = { { 0.0F, 0.0F, 0.0F }, { 0.0F, 0.0F, 0.0F }, { 0.0F, 0.0F, 0.0F }, { 0.0F, 0.0F, 0.0F },
				{ 0.0F, 0.0F, 0.0F }, { 0.0F, 0.0F, 0.0F } };

		try
		{
			scanner = new Scanner(new File("values.txt"));

			int i = 0;
			int j = 0;

			while (scanner.hasNextFloat())
			{
				if (i > 2)
				{
					i -= 3;
					j++;
				}
				colors[j][i++] = scanner.nextFloat();
			}

			scanner.close();
		}
		catch (FileNotFoundException e)
		{
			PrintString.print("Run 'WriteColorValues' first!", true);
			return null;
		}

		return colors;
	}
}
