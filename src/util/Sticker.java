package util;

public class Sticker
{
	private int surfaceNumber;
	private int stickerNumber;

	public Sticker(int surfaceNumber, int stickerNumber)
	{
		this.surfaceNumber = surfaceNumber;
		this.stickerNumber = stickerNumber;
	}

	public void set(int surfaceNumber, int stickerNumber)
	{
		this.surfaceNumber = surfaceNumber;
		this.stickerNumber = stickerNumber;
	}

	/**
	 * Apply virtual rotation to this sticker
	 * @param notation
	 */
	public void applyRotation(String notation)
	{
		//Up
		if (notation.equals("U") || notation.equals("U'"))
		{
			if (surfaceNumber == 0)
			{
				applySurfaceRotation(notation.equals("U"));
				return;
			}
			if (surfaceNumber == 1 && (stickerNumber == 0 || stickerNumber == 3 || stickerNumber == 6))
			{
				if (notation.equals("U")) surfaceNumber = 5;
				else surfaceNumber = 4;
				if (stickerNumber == 0) stickerNumber = 8;
				else if (stickerNumber == 3) stickerNumber = 5;
				else if (stickerNumber == 6) stickerNumber = 2;
				return;
			}
			if (surfaceNumber == 3 && (stickerNumber == 2 || stickerNumber == 5 || stickerNumber == 8))
			{
				if (notation.equals("U")) surfaceNumber = 4;
				else surfaceNumber = 5;
				return;
			}
			if (surfaceNumber == 4 && (stickerNumber == 2 || stickerNumber == 5 || stickerNumber == 8))
			{
				if (notation.equals("U"))
				{
					surfaceNumber = 1;
					if (stickerNumber == 8) stickerNumber = 0;
					else if (stickerNumber == 5) stickerNumber = 3;
					else if (stickerNumber == 2) stickerNumber = 6;
				}
				else surfaceNumber = 3;
				return;
			}
			if (surfaceNumber == 5 && (stickerNumber == 2 || stickerNumber == 5 || stickerNumber == 8))
			{
				if (notation.equals("U")) surfaceNumber = 3;
				else
				{
					surfaceNumber = 1;
					if (stickerNumber == 8) stickerNumber = 0;
					else if (stickerNumber == 5) stickerNumber = 3;
					else if (stickerNumber == 2) stickerNumber = 6;
				}
				return;
			}
		}

		//Down
		if (notation.equals("D") || notation.equals("D'"))
		{
			if (surfaceNumber == 2)
			{
				applySurfaceRotation(notation.equals("D"));
				return;
			}
			if (surfaceNumber == 1 && (stickerNumber == 2 || stickerNumber == 5 || stickerNumber == 8))
			{
				if (notation.equals("D")) surfaceNumber = 4;
				else surfaceNumber = 5;
				if (stickerNumber == 2) stickerNumber = 6;
				else if (stickerNumber == 5) stickerNumber = 3;
				else if (stickerNumber == 8) stickerNumber = 0;
				return;
			}
			if (surfaceNumber == 3 && (stickerNumber == 0 || stickerNumber == 3 || stickerNumber == 6))
			{
				if (notation.equals("D")) surfaceNumber = 5;
				else surfaceNumber = 4;
				return;
			}
			if (surfaceNumber == 4 && (stickerNumber == 0 || stickerNumber == 3 || stickerNumber == 6))
			{
				if (notation.equals("D")) surfaceNumber = 1;
				else
				{
					surfaceNumber = 3;
					if (stickerNumber == 0) stickerNumber = 8;
					else if (stickerNumber == 3) stickerNumber = 5;
					else if (stickerNumber == 6) stickerNumber = 2;
				}
				return;
			}
			if (surfaceNumber == 5 && (stickerNumber == 0 || stickerNumber == 3 || stickerNumber == 6))
			{
				if (notation.equals("D"))
				{
					surfaceNumber = 1;
					if (stickerNumber == 0) stickerNumber = 8;
					else if (stickerNumber == 3) stickerNumber = 5;
					else if (stickerNumber == 6) stickerNumber = 2;
				}
				else surfaceNumber = 3;
				return;
			}
		}

		//Left
		if (notation.equals("L") || notation.equals("L'"))
		{
			if (surfaceNumber == 3)
			{
				applySurfaceRotation(notation.equals("L"));
				return;
			}
			if (surfaceNumber == 2 && (stickerNumber == 2 || stickerNumber == 5 || stickerNumber == 8))
			{
				if (notation.equals("L"))
				{
					surfaceNumber = 4;
					if (stickerNumber == 2) stickerNumber = 8;
					else if (stickerNumber == 5) stickerNumber = 7;
					else if (stickerNumber == 8) stickerNumber = 6;
				}
				else
				{
					surfaceNumber = 5;
					if (stickerNumber == 2) stickerNumber = 0;
					else if (stickerNumber == 5) stickerNumber = 1;
					else if (stickerNumber == 8) stickerNumber = 2;
				}
				return;
			}
			if (surfaceNumber == 0 && (stickerNumber == 0 || stickerNumber == 3 || stickerNumber == 6))
			{
				if (notation.equals("L"))
				{
					surfaceNumber = 5;
					if (stickerNumber == 0) stickerNumber = 2;
					else if (stickerNumber == 3) stickerNumber = 1;
					else if (stickerNumber == 6) stickerNumber = 2;
				}
				else
				{
					surfaceNumber = 4;
					if (stickerNumber == 0) stickerNumber = 6;
					else if (stickerNumber == 3) stickerNumber = 7;
					else if (stickerNumber == 6) stickerNumber = 8;
				}
				return;
			}
			if (surfaceNumber == 4 && (stickerNumber == 6 || stickerNumber == 7 || stickerNumber == 8))
			{
				if (notation.equals("L"))
				{
					surfaceNumber = 0;
					if (stickerNumber == 6) stickerNumber = 0;
					else if (stickerNumber == 7) stickerNumber = 3;
					else if (stickerNumber == 8) stickerNumber = 6;
				}
				else
				{
					surfaceNumber = 2;
					if (stickerNumber == 6) stickerNumber = 8;
					else if (stickerNumber == 7) stickerNumber = 5;
					else if (stickerNumber == 8) stickerNumber = 2;
				}
				return;
			}
			if (surfaceNumber == 5 && (stickerNumber == 0 || stickerNumber == 1 || stickerNumber == 2))
			{
				if (notation.equals("L"))
				{
					surfaceNumber = 2;
					if (stickerNumber == 0) stickerNumber = 2;
					else if (stickerNumber == 1) stickerNumber = 5;
					else if (stickerNumber == 2) stickerNumber = 8;
				}
				else
				{
					surfaceNumber = 0;
					if (stickerNumber == 0) stickerNumber = 6;
					else if (stickerNumber == 1) stickerNumber = 3;
					else if (stickerNumber == 2) stickerNumber = 0;
				}
				return;
			}
		}

		//Right
		if (notation.equals("R") || notation.equals("R'"))
		{
			if (surfaceNumber == 1)
			{
				applySurfaceRotation(notation.equals("R"));
				return;
			}
			if (surfaceNumber == 2 && (stickerNumber == 0 || stickerNumber == 3 || stickerNumber == 6))
			{
				if (notation.equals("R"))
				{
					surfaceNumber = 5;
					if (stickerNumber == 0) stickerNumber = 6;
					else if (stickerNumber == 3) stickerNumber = 7;
					else if (stickerNumber == 6) stickerNumber = 8;
				}
				else
				{
					surfaceNumber = 4;
					if (stickerNumber == 0) stickerNumber = 2;
					else if (stickerNumber == 3) stickerNumber = 1;
					else if (stickerNumber == 6) stickerNumber = 0;
				}
				return;
			}
			if (surfaceNumber == 0 && (stickerNumber == 2 || stickerNumber == 5 || stickerNumber == 8))
			{
				if (notation.equals("R"))
				{
					surfaceNumber = 4;
					if (stickerNumber == 2) stickerNumber = 0;
					else if (stickerNumber == 5) stickerNumber = 1;
					else if (stickerNumber == 8) stickerNumber = 2;
				}
				else
				{
					surfaceNumber = 5;
					if (stickerNumber == 2) stickerNumber = 8;
					else if (stickerNumber == 5) stickerNumber = 7;
					else if (stickerNumber == 8) stickerNumber = 6;
				}
				return;
			}
			if (surfaceNumber == 4 && (stickerNumber == 0 || stickerNumber == 1 || stickerNumber == 2))
			{
				if (notation.equals("R"))
				{
					surfaceNumber = 2;
					if (stickerNumber == 0) stickerNumber = 6;
					else if (stickerNumber == 1) stickerNumber = 3;
					else if (stickerNumber == 2) stickerNumber = 0;
				}
				else
				{
					surfaceNumber = 0;
					if (stickerNumber == 0) stickerNumber = 2;
					else if (stickerNumber == 1) stickerNumber = 5;
					else if (stickerNumber == 2) stickerNumber = 8;
				}
				return;
			}
			if (surfaceNumber == 5 && (stickerNumber == 6 || stickerNumber == 7 || stickerNumber == 8))
			{
				if (notation.equals("R"))
				{
					surfaceNumber = 0;
					if (stickerNumber == 6) stickerNumber = 8;
					else if (stickerNumber == 7) stickerNumber = 5;
					else if (stickerNumber == 8) stickerNumber = 2;
				}
				else
				{
					surfaceNumber = 2;
					if (stickerNumber == 6) stickerNumber = 0;
					else if (stickerNumber == 7) stickerNumber = 3;
					else if (stickerNumber == 8) stickerNumber = 6;
				}
				return;
			}
		}

		//Front
		if (notation.equals("F") || notation.equals("F'"))
		{
			if (surfaceNumber == 5)
			{
				applySurfaceRotation(notation.equals("F"));
				return;
			}
			for (int i = 0; i < 4; i++)
			{
				if (surfaceNumber == i && (stickerNumber == 6 || stickerNumber == 7 || stickerNumber == 8))
				{
					int n = i;
					if (notation.equals("F"))
					{
						n++;
						if (n > 3) n = 0;
						surfaceNumber = n;
						return;
					}
					else
					{
						n--;
						if (n < 0) n = 3;
						surfaceNumber = n;
						return;
					}
				}
			}
		}

		//Back
		if (notation.equals("B") || notation.equals("B'"))
		{
			if (surfaceNumber == 4)
			{
				applySurfaceRotation(notation.equals("B"));
				return;
			}
			for (int i = 0; i < 4; i++)
			{
				if (surfaceNumber == i && (stickerNumber == 0 || stickerNumber == 1 || stickerNumber == 2))
				{
					int n = i;
					if (notation.equals("B'"))
					{
						n++;
						if (n > 3) n = 0;
						surfaceNumber = n;
						return;
					}
					else
					{
						n--;
						if (n < 0) n = 3;
						surfaceNumber = n;
						return;
					}
				}
			}
		}
	}

	private void applySurfaceRotation(boolean clockwise)
	{
		if (clockwise)
		{
			//Corner
			if (stickerNumber == 0) stickerNumber = 2;
			else if (stickerNumber == 2) stickerNumber = 8;
			else if (stickerNumber == 8) stickerNumber = 6;
			else if (stickerNumber == 6) stickerNumber = 0;
			//Edges
			else if (stickerNumber == 1) stickerNumber = 5;
			else if (stickerNumber == 5) stickerNumber = 7;
			else if (stickerNumber == 7) stickerNumber = 3;
			else if (stickerNumber == 3) stickerNumber = 0;
		}
		else
		{
			//Corner
			if (stickerNumber == 2) stickerNumber = 0;
			else if (stickerNumber == 8) stickerNumber = 2;
			else if (stickerNumber == 6) stickerNumber = 8;
			else if (stickerNumber == 0) stickerNumber = 6;
			//Edges
			else if (stickerNumber == 5) stickerNumber = 1;
			else if (stickerNumber == 7) stickerNumber = 5;
			else if (stickerNumber == 3) stickerNumber = 7;
			else if (stickerNumber == 0) stickerNumber = 3;
		}
	}

	public int getSurfaceNumber()
	{
		return surfaceNumber;
	}

	public int getStickerNumber()
	{
		return stickerNumber;
	}

	public boolean onEdgePiece()
	{
		if (stickerNumber % 2 == 1) return true;
		return false;
	}

	public boolean onCornerPiece()
	{
		if (stickerNumber % 2 == 1 || stickerNumber == 4) return false;
		return true;
	}

	public boolean onCenterPiece()
	{
		return stickerNumber == 4;
	}

	public boolean isAt(int surfaceNumber, int stickerNumber)
	{
		return (this.surfaceNumber == surfaceNumber) && (this.stickerNumber == stickerNumber);
	}

	/**
	 * @return True if this sticker is on the upper layer (not only surfaceNumber == 0)
	 */
	public boolean isOnUpperLayer()
	{
		if (this.getSurfaceNumber() == 0) return true;

		return this.isAt(1, 0) || this.isAt(1, 3) || this.isAt(1, 6) || this.isAt(3, 2) || this.isAt(3, 5)
				|| this.isAt(3, 8) || this.isAt(4, 2) || this.isAt(4, 5) || this.isAt(4, 8) || this.isAt(5, 2)
				|| this.isAt(5, 5) || this.isAt(5, 8);
	}

	/**
	 * @return True if this sticker is on the bottom layer (not only surfaceNumber == 2)
	 */
	public boolean isOnBottomLayer()
	{
		if (this.getSurfaceNumber() == 2) return true;

		return this.isAt(1, 2) || this.isAt(1, 5) || this.isAt(1, 8) || this.isAt(3, 0) || this.isAt(3, 3)
				|| this.isAt(3, 6) || this.isAt(4, 0) || this.isAt(4, 3) || this.isAt(4, 6) || this.isAt(5, 0)
				|| this.isAt(5, 3) || this.isAt(5, 6);
	}

	/**
	 * @return True if this sticker is on the middle layer
	 */
	public boolean isOnMiddleLayer()
	{
		return !isOnUpperLayer() && !isOnBottomLayer();
	}
}
