package util;

import solve.Solve;

public class TestSolveFunction
{
	public static void test(int amountOfCubesToSolve)
	{
		PrintString.print("Press ESC to start!", true);
		String allSolutions = "";
		long start = System.nanoTime();

		for (int i = 0; i < amountOfCubesToSolve; i++)
		{
			//Solved position
			String[][] position = { { "w", "w", "w", "w", "w", "w", "w", "w", "w" },
					{ "r", "r", "r", "r", "r", "r", "r", "r", "r" }, { "y", "y", "y", "y", "y", "y", "y", "y", "y" },
					{ "o", "o", "o", "o", "o", "o", "o", "o", "o" }, { "b", "b", "b", "b", "b", "b", "b", "b", "b" },
					{ "g", "g", "g", "g", "g", "g", "g", "g", "g" } };

			Cube cube = new Cube(position);
			String scramble = ScrambleCube.scramble(cube);

			String currentSolution = Solve.solve(cube); //Calculate solution and add it to the string
			//Failed to solve
			if (currentSolution == null)
			{
				PrintString.print("Failed to solve this scramble: " + scramble, true);
				PrintString.printCube(cube);
				return;
			}
			allSolutions += currentSolution;
		}

		long end = System.nanoTime();
		double deltaT = (end - start) * 0.000000001;
		PrintString.print("Solved " + amountOfCubesToSolve + " cubes successfully!", true);
		PrintString.print("Time: " + deltaT, true);

		int solutionLength = 0;

		for (int i = 0; i < allSolutions.length(); i++)
		{
			if (allSolutions.charAt(i) == 'U' || allSolutions.charAt(i) == 'D' || allSolutions.charAt(i) == 'L'
					|| allSolutions.charAt(i) == 'R' || allSolutions.charAt(i) == 'F' || allSolutions.charAt(i) == 'B')
				solutionLength++;
		}

		int avgSolutionLength = solutionLength / amountOfCubesToSolve;
		PrintString.print("Avg moves: " + avgSolutionLength, true);
	}
}
