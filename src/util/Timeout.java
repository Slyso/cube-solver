package util;

public class Timeout
{
	private static long timedOutAt;

	/**
	 * If timeOutInMs has passed, timedOut() will return true.
	 * @param timeOutInMs
	 */
	public static void timedOutIn(long timeOutInMs)
	{
		timedOutAt = System.currentTimeMillis() + timeOutInMs;
	}

	public static boolean timedOut()
	{
		if (timedOutAt - System.currentTimeMillis() <= 0) return true;
		return false;
	}
}
