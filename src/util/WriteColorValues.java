package util;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import movement.MoveArm;
import movement.MoveColorSensor;
import sensor.ColorSensor;

public class WriteColorValues
{
	/**
	 * Saves the color values of the stickers on the cube into a file
	 * @param args
	 */
	public static void main(String[] args)
	{
		String[] order = { "blue", "green", "orange", "red", "white", "yellow" };
		float[][] colors = { { 0.0F, 0.0F, 0.0F }, { 0.0F, 0.0F, 0.0F }, { 0.0F, 0.0F, 0.0F }, { 0.0F, 0.0F, 0.0F },
				{ 0.0F, 0.0F, 0.0F }, { 0.0F, 0.0F, 0.0F } };

		resetMotorPositions();
		MoveColorSensor.move();
		ColorSensor.start();

		try
		{
			Writer writer = new FileWriter("values.txt");

			//Make measurements
			for (int i = 0; i < colors.length; i++)
			{
				PrintString.print("Put a " + order[i] + " sticker under the color sensor and press escape", true);

				float[] rgb = ColorSensorUtil.getRGBRange(50);
				colors[i][0] = rgb[6];
				colors[i][1] = rgb[7];
				colors[i][2] = rgb[8];
			}

			//Write measurements to the file
			for (int i = 0; i < colors.length; i++)
			{
				for (int j = 0; j < colors[i].length; j++)
				{
					writer.write(Float.toString(colors[i][j]) + " ");
				}
			}

			writer.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private static void resetMotorPositions()
	{
		//Button.LEDPattern(2); //Red static
		MoveColorSensor.reset();
		MoveArm.reset();
	}
}
